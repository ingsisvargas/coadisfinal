<%-- 
    Document   : accesoEstudiante
    Created on : 31-mar-2014, 8:23:34
    Author     : SoftVargas
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link type="text/css" rel="stylesheet" href="../CSS/estilo.css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Estudiante</title>
    </head>
    <body>
         <%
        
          if(session.getAttribute("codigoE")==null){
               %>
        <script type="text/javascript">
            alert("DEBE INICIAR SESION EN EL SISTEMA");
            window.location = "../index.html";
        </script>

        <% }
        
        
        
        %>
    <center>
        <div id="contenedorPadre">

            <div id="banner">
                <img src="..//IMG/banner.png"/>
            </div>
        
        <br>
        <div id="contenedorB">
            <h1>CONSULTA DE HORARIOS</h1><br><br>
            <div id="contenedorC">

                <div class="main_cont"> 
                    <div class="menu_top_bg">MENU DE OPCIONES</div> 

                    <div class="menu_top_bg"> HORARIO ASESORIAS </div> 
                    <div class="sub_menu">
                        <ul>
                            <li><a href="#">1150605 -  Analisis y Diseño De Sistemas</a></li>
                            <li><a href="#">1150612 -  Administracion De proyectos Informaticos</a></li>
                            <li><a href="#">1150704 -  Teoria General De Las Comunicaciones</a></li>
                            <li><a href="#">1150924 -  Desarrollo De Aplicaciones Web</a></li>
                            <li><a href="#">1156701 -  Patrones De Diseño</a></li>
                        </ul>


                    </div>


                </div>


            </div>

            <div id="contenedorD"></div>

        </div>


        <a id="cierre" href="../JSP/cerrar.jsp">Cerrar Sesion</a>
        
        </div>
    </center>
</body>
</html>
