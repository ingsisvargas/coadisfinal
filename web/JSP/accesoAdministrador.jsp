
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link type="text/css" rel="stylesheet" href="../CSS/estilo.css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/JavaScript" src="../JS/funciones.js"></script>
        <script type="text/JavaScript" src="../JS/jquery-1.11.1.js"></script>
        <script type="text/JavaScript" src="../JS/jquery-ui-1.10.4.custom.min.js"></script>


        <title>Acceso Administrador</title>
        <script type="text/javascript">
            window.onload = function() {
                setInterval(muestraReloj, 1000);
            };
        </Script>
    </head>
    <body>

        <%

            if (session.getAttribute("codigoA") == null) {
        %>
        <script type="text/javascript">
            alert("DEBE INICIAR SESION EN EL SISTEMA");
            window.location = "../index.html";
        </script>

        <% }

            String nombre = session.getAttribute("nombreUser").toString();


        %>
    <center>
        <div id="contenedorPadre">


            <div id="banner">
                <img src="../IMG/bannerA.png"/>
            </div>
            <br/>
            <br/>
            <div id="login" class="tresEnLinea">
                <h3>En Linea<br/><%=nombre%></h3>
            </div>
            <div class="tresEnLinea">
                <h1>ACCESO ADMINISTRADOR</h1>
            </div>
            <div id="reloj" class="tresEnLinea">
                <br/> <h3>Hora Sistema<br/></h3>
            </div>
            <br/>
            <div id="contenedorInforme">
                <br/>
                <div class="dosEnLinea">
                    <div id="contenedorC">

                        <div class="main_cont"> 
                            <div class="menu_top_bg">MENU DE OPCIONES</div> 

                            <div class="menu_top_bg">CONSULTAS</div> 
                            <div class="sub_menu">
                                <ul>
                                    <li><a href="#" onclick="cargarConsultaHorarioDocente();">Consultar Horario Docente</a></li>
                                </ul>
                            </div>

                            <div class="menu_top_bg">Actualizacion</div> 
                            <div class="sub_menu">
                                <ul>
                                    <li><a href="#" onclick="cargarActualizacionSemestral();">Cargar Base de Datos</a></li>
                                    <li><a href="#" onclick="cargarCambiarContrasena(1);">Cambiar Contraseña</a></li>

                                </ul>
                            </div>

                            <div class="menu_top_bg"></div>
                            <div class="menu_top_bg">PERMISOS</div>
                            <div class="sub_menu">
                                <ul>
                                    <li><a href="#" onclick="cargarCambioDeEstado();">Habilitar Docente Para Registro</a></li> </ul> </div>
                            <div class="menu_top_bg"></div> 
                            <div class="menu_top_bg">REPORTES</div>
                            <div class="sub_menu">
                                <ul>
                                    <li><a href="#" onclick="cargarGenerarReporteGeneralPlanta();">Reporte General Docentes Planta</a></li>
                                    <li><a href="#" onclick="cargarGenerarReporteGeneralCatedra();">Reporte General Docentes Catedra</a></li>
                                    <li><a href="#" onclick="cargarGenerarReporteDocente();">Reporte Indicadores de Uso</a></li>
                                    <li><a href="#" onclick="cargarGenerarReporteRetardo();">Reporte General Retardos Docente </a></li>
                                </ul> </div>


                        </div>

                    </div>
                </div>
                <div class="dosEnLinea" id="contenedorCarga">

                    <div id="imgDoc">
                        <br/><br/>
                        <img id="imgP" src="../IMG/admin.jpg"/>
                    </div>
                </div>
            </div>
            <br/>

            <a id="cierre" href="../JSP/cerrar.jsp">Cerrar Sesion</a>
            <br/><br/>
        </div>
        <br/>
    </center>
</body>
</html>



