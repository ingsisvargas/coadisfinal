
<%-- 
    Document   : accesoModulo
    Created on : 31-mar-2014, 8:21:12
    Author     : SoftVargas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="fachada" scope="page" class="Coadis.Facade.Facade" />
<jsp:setProperty name="fachada" property="*"></jsp:setProperty>
<jsp:useBean id="per" class="Coadis.DTO.Persona" />
<jsp:setProperty name="per" property="*"/>

<!DOCTYPE html>
<html>
    <head>
        <link type="text/css" rel="stylesheet" href="CSS/estilo.css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ingresando</title>
    </head>
    <body>

        <%

            int tip = Integer.parseInt(request.getParameter("tipo"));
            Integer tipo = new Integer(tip);
            String r = fachada.validarIngreso(per, tipo);
            String res[] = r.split("%%");
            request.getSession(true);

            if (Integer.parseInt(res[0]) == 0) {
        %>
        <script type="text/javascript">
            alert("ERROR.. DATOS NO VALIDOS");
            window.location = "../index.html";
        </script>

        <% }
            if (Integer.parseInt(res[0]) == 1) {

                session.setAttribute("tipo", tipo);
                session.setAttribute("codigoA", per.getCodigo());
                session.setAttribute("pass", per.getClave());
                session.setAttribute("nombreUser", res[1]);
        %>
        <script type="text/javascript">
            alert("Bienvenido Administrador");
            window.location = "../JSP/accesoAdministrador.jsp";
        </script>


        <% }

            if (Integer.parseInt(res[0]) == 2) {
                session.setAttribute("tipo", tipo);
                session.setAttribute("codigoD", per.getCodigo());
                session.setAttribute("nombreUser", res[1]);

                if (fachada.validarRetraso(per)) {%>
        <script type="text/javascript">
            alert("BIENVENIDO.. SE HA GENERADO UN RETRASO DEBIDO A SU HORA DE LLEGADA.");
            window.location = "../JSP/accesoDocente.jsp";
        </script>

        <%} else {
        %>
        <script type="text/javascript">
            alert("Bienvenido Docente");
            window.location = "../JSP/accesoDocente.jsp";
        </script>


        <% }
            }
            %>
    </body>
</html>
