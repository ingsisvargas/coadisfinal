<%-- 
    Document   : servicioPrueba
    Created on : 15-may-2014, 15:19:42
    Author     : Rosemberg
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="per" scope="page" class="Coadis.DTO.Persona"/>
<jsp:useBean id="fachada" scope="page" class="Coadis.Facade.Facade"/>
<jsp:useBean id="mat" scope="page" class="Coadis.DTO.Materia"/>
<jsp:useBean id="grup" scope="page" class="Coadis.DTO.Grupo"/>
<jsp:useBean id="doc" scope="page" class="Coadis.DTO.Docente"/>
<jsp:useBean id="tema" scope="page" class="Coadis.DTO.Tema"/>
<jsp:useBean id="admin" scope="page" class="Coadis.DTO.Administrador"/>


<%

    String tipo=request.getParameter("tipo");
    
    String codigoProfe=request.getParameter("codigo");
    
    per.setCodigo(codigoProfe);
    
    doc.setCodigo(codigoProfe);
    
    if(tipo.equals(0)){
    
        String[]datos=fachada.getDatos(per);

        if(datos==null)
            out.print("Datos no encontrados");

        else
        out.println("codigo: "+datos[0]+"-"+"nombre: "+datos[1]+"-"+"Documento: "+datos[2]);
    
    }
    else if(tipo.equals("1")){
        String materia=request.getParameter("materia");
        mat.setCodigoM(materia);
       
        String html=fachada.cargarGrupos(mat);
         
            out.print(html);
        
    }
    else if(tipo.equals("2")){
        
        int tem=Integer.parseInt(request.getParameter("tema"));
        tema.setId(tem);
        
        String codigoEstudiante=request.getParameter("codigoE");
        
        String observacion=request.getParameter("descripcion");
       
        int r=fachada.registrarAsesoria(codigoEstudiante,tema, observacion);
        
        if(r>0){
            doc.setCodigo(session.getAttribute("codigoD").toString());
            String asesorias=fachada.cargarAsesoriasDia(doc);
            out.print("Registro Exitoso!"+"RAVM"+asesorias);
        }
        
        else
        {
            out.print("Fallo en Registro");
        }
        
    }
    else if(tipo.equals("3")){
        
       out.print(fachada.cargarMesesDocente(doc));
       
    }
    
    else if(tipo.equals("4")){
        
        out.print(fachada.cargarMesesRetardos(doc));
    }
    
    else if(tipo.equals("5")){
        String pass=request.getParameter("pass");
        doc.setCodigo(session.getAttribute("codigoD").toString());
        out.print(fachada.verificarContraseña(doc, pass));
    }
    
     else if(tipo.equals("6")){
         String user=request.getParameter("user");
        String pass=request.getParameter("pass");
        
        if(user=="0"){
        doc.setCodigo(session.getAttribute("cod").toString());
        out.print(fachada.cambiarContrasena(doc, pass));
    }else{
           admin.setCodigo(session.getAttribute("cod").toString());
        out.print(fachada.cambiarContrasena(admin, pass));
        }
     
     }
     else if(tipo.equals("7")){
        int gru=Integer.parseInt(request.getParameter("grupo"));
        grup.setId(gru);
        out.print(fachada.cargarTemas(grup));
        
     }
     
    else out.print(codigoProfe);
     %>