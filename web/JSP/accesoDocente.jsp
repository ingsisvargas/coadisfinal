<%-- 
    Document   : accesoDocente
    Created on : 31-mar-2014, 8:23:15
    Author     : SoftVargas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link type="text/css" rel="stylesheet" href="../CSS/estilo.css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/JavaScript" src="../JS/funciones.js"></script>
        <script type="text/JavaScript" src="../JS/jquery-1.11.1.js"></script>
        <script type="text/JavaScript" src="../JS/jquery-ui-1.10.4.custom.min.js"></script>



        <title>Acceso Docente</title>
        <script type="text/javascript">
            window.onload = function() {
                setInterval(muestraReloj, 1000);
            };
        </Script>

    </head>
    <body>
        <%

            if (session.getAttribute("codigoD") == null) {
        %>
        <script type="text/javascript">
            alert("DEBE INICIAR SESION EN EL SISTEMA");
            window.location = "../index.html";
        </script>

        <% }

            String nombre = session.getAttribute("nombreUser").toString();

        %>

    <center>
        <div id="contenedorPadre">


            <div id="banner">
                <img src="..//IMG/bannerA.png"/>
            </div>

            <br/>
            <br/>
            <div id="login" class="tresEnLinea">
                <h3>En Linea<br/><%=nombre%></h3>
            </div>
            <div class="tresEnLinea">
                <h1>ACCESO DOCENTE</h1>
            </div>
            <div id="reloj" class="tresEnLinea">
                <h3>Hora Sistema<br/></h3>
            </div>
            <br/>
            <div id="contenedorInforme">
                <br/>
                <div class="dosEnLinea">
                    <div id="contenedorC">

                        <div class="main_cont"> 
                            <div class="menu_top_bg">MENU DE OPCIONES</div> 

                            <div class="menu_top_bg">REGISTROS</div> 
                            <div class="sub_menu">
                                <ul>
                                    <li><a href="#" onclick="cargarRegistroHorario();">Registrar Horario De Asesoria</a></li>
                                    <li><a href="#" onclick="cargarRgistroAsesoria();">Registrar Asesoria</a></li>
                                    <li><a href="#" onclick="cargarRegistroTema();">Registrar Tema de Asesoria</a></li>
                                    <li><a href="#" onclick="cargarCambiarContrasena(0);">Cambiar Contraseña</a></li>
                                </ul>
                            </div>
                            <div class="menu_top_bg"></div>
                            <div class="menu_top_bg">REPORTES</div>
                            <div class="sub_menu">
                                <ul>
                                    <li><a href="#" onclick="cargarReporteDetalladoSemanaDocente();">Reporte Indicadores de Uso</a></li> </ul> </div>

                        </div>

                    </div>
                </div>


                <div class="dosEnLinea" id="contenedorCarga">
                    <div id="imgDoc">

                        <img id="imgP" src="..//IMG/senor.gif"/>


                    </div>
                </div>

            </div>
            <br/>

            <a id="cierre" href="../JSP/cerrar.jsp">Cerrar Sesion</a>
            <br/><br/>
        </div>
        <br/>
    </center>
</body>
</html>
