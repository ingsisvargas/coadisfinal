<%-- 
    Document   : generadorReporte
    Created on : 8/05/2014, 04:05:09 PM
    Author     : Luis Miguel Blanco
--%>
<%@page import="com.itextpdf.text.*"%>
<%@page import="com.itextpdf.text.pdf.*"%>

<jsp:useBean id="fachada" class="Coadis.Facade.Facade"></jsp:useBean>
<jsp:useBean id="doc" class="Coadis.DTO.Docente"></jsp:useBean>
<jsp:useBean id="mat" class="Coadis.DTO.Materia"></jsp:useBean>

        
<%@ page  language="java" import="java.io.*,java.sql.*,java.awt.Color" contentType="text/html; charset=utf-8"  errorPage=""  %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>REPORTE</title>
    </head>
    <body>
       <%

    String rutaLocal = request.getSession().getServletContext().getRealPath("");
    
    String mes=request.getParameter("mes");
    
    if(mes.equals("0")){
        
        %>
    
        <script type="text/javascript">
        alert("No puede generar el reporte ");
        window.location = "../JSP/accesoAdministrador.jsp";
        </script>
    
     <%          } 
    response.setContentType("application/pdf");
    
    
    
    Document document = new Document();
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    PdfWriter.getInstance(document, response.getOutputStream());
    
    Image foto = Image.getInstance(rutaLocal+"/IMG/bannerA.png");
	foto.scaleToFit(500,200);
	foto.setAlignment(Chunk.ALIGN_MIDDLE);
    
    fachada.generarPDFMesualCatedra(document, mes, foto);
    
    DataOutput output = new DataOutputStream(response.getOutputStream());
    byte[] bytes = buffer.toByteArray();
    response.setContentLength(bytes.length);
    for (int j = 0; j < bytes.length; j++) {
        output.writeByte(bytes[j]);
    }

%>
    </body>
</html>
