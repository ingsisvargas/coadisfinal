/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




/**
 * Permite definir el reloj que marca la hora actual del sistema en la pagina.
 * @returns {undefined}
 */
function muestraReloj() {
    var fechaHora = new Date();
    var horas = fechaHora.getHours();
    var minutos = fechaHora.getMinutes();
    var segundos = fechaHora.getSeconds();

    if (horas < 10) {
        horas = '0' + horas;
    }
    if (minutos < 10) {
        minutos = '0' + minutos;
    }
    if (segundos < 10) {
        segundos = '0' + segundos;
    }

    document.getElementById("reloj").innerHTML = '<h3>Hora Sistema<br/> ' + horas + ':' + minutos + ':' + segundos + '</h3>';
}

/**
 * Permite gestionar el cambio de estado de un docente para el registro
 * de horarios
 * @returns {undefined}
 */
function cambiarEstado() {
    var cod = document.getElementById("codigo").value;
    var accion = document.getElementById("accion").value;

    if (cod === "") {
        alert("Debe Ingresar el Codigo del Docente!!");
        return;
    }
    if (accion === -1) {
        alert("Debe Indicar una accion a Realizar!!");
        return;
    }

    ajax = nuevoAjax();
    parametros = "codigo=" + cod + "&accion=" + accion;
    url = "../JSP/habilitacion.jsp";
    ajax.open("POST", url, true);
    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    ajax.send(parametros);

    ajax.onreadystatechange = function()
    {
        if (ajax.readyState == 4)
        {
            if (ajax.status == 200)
            {

                alert("Estado Cambiado a:\n" + ajax.responseText);
                window.location = "../JSP/accesoAdministrador.jsp";
            }

        }
        else
        {
        }
    };
}

function registrarTema() {


    if (document.form1.materia.selectedIndex === 0) {
        alert("Debe seleccionar una MATERIA.");
        document.form1.materia.focus();
        return false;
    }


    if (document.form1.grupo.selectedIndex === 0) {
        alert("Debe seleccionar un grupo.");
        document.form1.grupo.focus();
        return false;
    }

    if (document.form1.tema.value === "") {
        alert("Debe digitar un tema.");
        document.form1.tema.focus();
        return false;

    }
    //el formulario se envia 

    var mat = document.getElementById("materia").value;
    alert(mat + " Materia");
    var gru = document.getElementById("grupo").value;
    alert(mat + " Grupo" + gru);
    var tema = document.form1.tema.value;

    alert(mat + gru + tema);

    ajax = nuevoAjax();
    parametros = "materia=" + mat + "&grupo=" + gru + "&tema=" + tema;
    url = "../JSP/registroTema.jsp";
    ajax.open("POST", url, true);
    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    ajax.send(parametros);


    ajax.onreadystatechange = function()
    {
        if (ajax.readyState == 4)
        {
            if (ajax.status == 200)
            {
                alert(ajax.responseText);
            }

        }
        else
        {
        }
    };


    return true;
}

function cargarTemas() {
    var grupo = document.getElementById('grupo').value;

    ajax = nuevoAjax();
    parametros = "grupo=" + grupo + "&tipo=" + "7";
    url = "../JSP/servicioPrueba.jsp";
    ajax.open("POST", url, true);
    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    ajax.send(parametros);


    ajax.onreadystatechange = function()
    {
        if (ajax.readyState == 4)
        {
            if (ajax.status == 200)
            {
                var div = document.getElementById("tema");
                div.innerHTML = ajax.responseText;
            }

        }
        else
        {
        }
    };



}


function registrarAsesoria() {

    var tema = document.getElementById('tema').value;
    var descripcion = document.getElementById('descripcion').value;
    var codigoE = document.getElementById('codEstudiante').value;

    ajax = nuevoAjax();
    parametros = "tema=" + tema + "&descripcion=" + descripcion + "&codigoE=" + codigoE + "&tipo=" + "2";
    url = "../JSP/servicioPrueba.jsp";
    ajax.open("POST", url, true);
    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    ajax.send(parametros);


    ajax.onreadystatechange = function()
    {
        if (ajax.readyState == 4)
        {
            if (ajax.status == 200)
            {
                var data = ajax.responseText.split("RAVM");
                var div = document.getElementById("tablaAsesorias");
                alert(data[0]);
                document.getElementById('materia').selectedIndex = 0;
                document.getElementById('grupo').selectedIndex = 0;
                document.getElementById('tema').selectedIndex = 0;
                document.getElementById('descripcion').value = "";
                document.getElementById('codEstudiante').value = "";
                div.innerHTML = data[1];

            }

        }
        else
        {
        }
    };


}


function cerrarVentanaDescripcion() {
    $('.dialogo').dialog("destroy");
}

function visualizarDescripcionAsesoria(val) {

    $("#dialogo").dialog();

    if (document.getElementById("dialogo") !== null) {
        $(".dialogo").dialog("close");
        var divDialogo = document.getElementById("dialogo");
        divDialogo.parentNode.removeChild(divDialogo);
    }



    if ($('.dialogo').dialog("isOpen")) {

        var div = document.createElement("div");
        div.setAttribute("id", "dialogo");
        div.setAttribute("class", "dialogo");

        document.body.appendChild(div);

        var div2 = document.createElement("div");
        var text = "<h2>Descripcion De Asesoria</h2><textarea disabled='true' style='width:300px; height:170px; left:10%; position:relative;'>" + val + "</textarea><br/><br/>";
        div2.innerHTML = text;
        div.appendChild(div2);

        $("#dialogo").dialog({
            height: 280,
            width: 300
        });

    }
}

function verificarContraseña(codigo, tipo) {
    var anterior = document.getElementById("ante").value;

    alert("entro a verificar contrasena: " + anterior);

    ajax = nuevoAjax();
    parametros = "pass=" + anterior + "&tipo=" + "5";
    url = "../JSP/servicioPrueba.jsp";
    ajax.open("POST", url, true);
    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    alert("se enviara el ajax con: " + parametros);
    ajax.send(parametros);


    ajax.onreadystatechange = function()
    {
        if (ajax.readyState == 4)
        {
            if (ajax.status == 200)
            {
                alert(ajax.responseText);
                if (ajax.responseText === "true") {
                    cambiarContrasena(codigo + "" + tipo);

                }
                else {
                    return false;
                }
            }

        }
        else
        {
        }
    };
}


function cambiarContrasena(codigo, tipo) {


    //alert("contraseña: "+document.formContrasena.nueva.value.length);
//    if (document.formContrasena.nueva.value.length < 6) {
//        alert("Debe escribir una contraseña con minimo 6 caracteres");
//        return false;
//    }


    if (!(document.formContrasena.nueva.value === document.formContrasena.repita.value)) {
        alert("Las contraseñas No Coinciden");
        document.formContrasena.nueva.focus();
        return false;
    }



    //el formulario se envia 

    var pass = document.formContrasena.nueva.value;


    ajax = nuevoAjax();

    parametros = "pass=" + pass + "&cod=" + codigo + "&tipo=" + 6 + "&user=" + tipo;
    url = "../JSP/servicioPrueba.jsp";
    ajax.open("POST", url, true);
    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    ajax.send(parametros);


    ajax.onreadystatechange = function()
    {
        if (ajax.readyState == 4)
        {
            if (ajax.status == 200)
            {
                alert(ajax.responseText);
            }

        }
        else
        {
        }
    };


    return true;
}




function cargarGrupos() {

    var mat = document.getElementById("materia").value;
    if (mat === "0") {
        alert("Debe Seleccionar una Materia");

        var div = document.getElementById("grupo");
        div.innerHTML = "<option value=0>-seleccione grupo-</option>";

        return;

    }
    ajax = nuevoAjax();
    parametros = "materia=" + mat + "&tipo=" + "1";
    url = "../JSP/servicioPrueba.jsp";
    ajax.open("POST", url, true);
    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    ajax.send(parametros);


    ajax.onreadystatechange = function()
    {
        if (ajax.readyState == 4)
        {
            if (ajax.readyState == 4)
            {
                if (ajax.status == 200)
                {
                    var div = document.getElementById("grupo");
                    div.innerHTML = ajax.responseText;
                }

            }
            else
            {
            }
        }

    };
}

function cargarConsultaHorario() {
    document.getElementById("resConsulta").innerHTML = "";
    var cod = document.getElementById("codigoD").value;
    if (cod === "") {
        alert("Debe Ingresar El Codigo Docente");
        return;
    }

    ajax = nuevoAjax();
    parametros = "codigoD=" + cod;
    url = "../JSP/consultaHorario.jsp";
    ajax.open("POST", url, true);
    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    ajax.send(parametros);

    ajax.onreadystatechange = function()
    {
        if (ajax.readyState == 4)
        {
            if (ajax.status == 200)
            {
                var div = document.getElementById("resConsulta");
                div.innerHTML = ajax.responseText;
            }

        }
        else
        {
        }
    };

}

/**
 * Permite gestionar la carga de la base de datos conforme se da el inicio de un nuevo semestre
 * @returns {undefined}
 */
function cargarBd() {
    ajax = nuevoAjax();
    parametros = "anio=" + document.getElementById("anio").value + "&periodo=" + document.getElementById("periodo").value + "&pass=" + document.getElementById("pass").value;
    url = "../JSP/cargarConcreta.jsp";
    ajax.open("POST", url, true);
    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    ajax.send(parametros);
    var div = document.getElementById("contenedorD");
    div.innerHTML = "";
    div.innerHTML = "<br/><h2>Actualizacion De Datos Semestrales</h2><br/><br/><br/><br/><img src='../IMG/loading-red.gif'/><br/>Realizando Actualizacion...<br/>Por favor, Espere";
    ajax.onreadystatechange = function()
    {
        if (ajax.readyState == 4)
        {
            if (ajax.readyState == 4)
            {
                if (ajax.status == 200)
                {
                    alert(ajax.responseText);
                    window.location = "../JSP/accesoAdministrador.jsp";
                }

            }

        }
        else
        {
        }
    };
}

/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
 lo que se puede copiar tal como esta aqui */
function nuevoAjax()
{
    var xmlhttp = false;
    try {
        // Creacion del objeto AJAX para navegadores no IE Ejemplo:Mozilla, Safari 
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            // Creacion del objet AJAX para IE
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            if (!xmlhttp && typeof XMLHttpRequest != 'undefined')
                xmlhttp = new XMLHttpRequest();
        }
    }
    return xmlhttp;
}


/**
 * Permite validar si el numero de horas seleccionadas en la tabla es optimo
 * respecto a la cantidad de horas permitidas
 * @param {type} max
 * @returns {Boolean}
 */
function maximoChecked(id) {

    var val = document.getElementById(id);
    var grupo = document.getElementsByName("hora");
    var cuantosChecked = 0;

    for (var b = 0; b < grupo.length; b++)
        if (grupo[b].checked)
            cuantosChecked++;

    if (cuantosChecked > 6) {

        val.checked = false;
        alert("Lo sentimos!\nEl maximo de horas para asesorias es 6");
        return false;

    }
    return true;
}

function guardarEstadoHorarios() {
    var v = [];
    var grupo = document.getElementsByName("hora");
    var i = 0;
    for (var b = 0; b < grupo.length; b++)
        if (grupo[b].checked) {
            v[i] = grupo[b].value;
            i++;
        }

    ajax = nuevoAjax();
    parametros = "hora=" + v;
    url = "../JSP/registroConcretado.jsp";
    ajax.open("POST", url, true);
    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    ajax.send(parametros);

    ajax.onreadystatechange = function()
    {
        if (ajax.readyState == 4)
        {
            if (ajax.status == 200)
            {
                alert(ajax.responseText);
                window.location = "../JSP/accesoDocente.jsp";
            }

        }
        else
        {
        }
    };


}


/**
 ***********************************************************************************
 *FUNCIONES DE CARGA PARA LOS FORMULARIOS DE LAS ACCIONES REALIZADAS POR UN DOCENTE/
 ***********************************************************************************
 */
function cargarInicioDocente() {
    $("#contenedorCarga").load("../forms/accesoDocente/inicioDocente.html").hide().fadeIn(1000);
    $("#contenedorCarga").focus();
}

function cargarRegistroHorario() {
    $("#contenedorCarga").load("../forms/accesoDocente/registrarHorario.jsp").hide().fadeIn(1000);
    $("#contenedorCarga").focus();
}

function cargarRegistroTema() {
    $("#contenedorCarga").load("../forms/accesoDocente/registrarTema.jsp").hide().fadeIn(1000);
    $("#contenedorCarga").focus();
}

function cargarRgistroAsesoria() {
    $("#contenedorCarga").load("../forms/accesoDocente/registrarAsesoria.jsp").hide().fadeIn(1000);
    $("#contenedorCarga").focus();
}

function cargarReporteDetalladoSemanaDocente() {
    $("#contenedorCarga").load("../forms/accesoDocente/reporteDetalladoSemana.jsp").hide().fadeIn(1000);
    $("#contenedorCarga").focus();
}

/**
 ***********************************************************************************
 *FUNCIONES DE CARGA PARA LOS FORMULARIOS DE LAS ACCIONES REALIZADAS POR UN DOCENTE/
 ***********************************************************************************
 */
function cargarInicioDocente() {
    $("#contenedorCarga").load("../forms/accesoDocente/inicioDocente.html").hide().fadeIn(1000);
    $("#contenedorCarga").focus();
}

function cargarRegistroHorario() {
    $("#contenedorCarga").load("../forms/accesoDocente/registrarHorario.jsp").hide().fadeIn(1000);
    $("#contenedorCarga").focus();
}

function cargarRegistroTema() {
    $("#contenedorCarga").load("../forms/accesoDocente/registrarTema.jsp").hide().fadeIn(1000);
    $("#contenedorCarga").focus();
}

function cargarRgistroAsesoria() {
    $("#contenedorCarga").load("../forms/accesoDocente/registrarAsesoria.jsp").hide().fadeIn(1000);
    $("#contenedorCarga").focus();
}

function cargarReporteDetalladoSemanaDocente() {
    $("#contenedorCarga").load("../forms/accesoDocente/reporteDetalladoSemana.jsp").hide().fadeIn(1000);
    $("#contenedorCarga").focus();
}

function cargarCambiarContrasena(val) {
    if (val == 0) {
        $("#contenedorCarga").load("../forms/accesoDocente/cambioContrasena.jsp?val=" + val).hide().fadeIn(1000);
        $("#contenedorCarga").focus();
    }
    if (val == 1) {
        $("#contenedorCarga").load("../forms/accesoDocente/cambioContrasena.jsp?val=" + val).hide().fadeIn(1000);
        $("#contenedorCarga").focus();
    }
}

function cargarConsultaHorarioDocente() {
    $("#contenedorCarga").load("../forms/accesoAdministrador/horarioDocente.jsp").hide().fadeIn(1000);
}

function cargarActualizacionSemestral() {
    $("#contenedorCarga").load("../forms/accesoAdministrador/cargarBD.jsp").hide().fadeIn(1000);
}

function cargarCambioDeEstado() {
    $("#contenedorCarga").load("../forms/accesoAdministrador/cambiarEstado.jsp").hide().fadeIn(1000);
}

function cargarGenerarReporteGeneralPlanta() {
    $("#contenedorCarga").load("../forms/accesoAdministrador/reporteGeneralPlanta.jsp").hide().fadeIn(1000);
}

function cargarGenerarReporteGeneralCatedra() {
    $("#contenedorCarga").load("../forms/accesoAdministrador/reporteGeneralCatedra.jsp").hide().fadeIn(1000);
}

function cargarGenerarReporteDocente() {
    $("#contenedorCarga").load("../forms/accesoAdministrador/reporteAsesoriaDocente.jsp").hide().fadeIn(1000);
}

function cargarGenerarReporteRetardo() {
    $("#contenedorCarga").load("../forms/accesoAdministrador/reporteRetardos.jsp").hide().fadeIn(1000);

}
