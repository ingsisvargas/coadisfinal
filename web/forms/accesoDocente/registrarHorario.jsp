
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="fachada" scope="page" class="Coadis.Facade.Facade"/>
<jsp:useBean id="doc" scope="page" class="Coadis.DTO.Docente"/>
<%

    doc.setCodigo(session.getAttribute("codigoD").toString());
    String filas = fachada.getHorarioAsesoria(doc);


%>

<div id="contenedorE">
    <h3>Gestion De Horarios</h3>
    <h5>Seleccione la(s) Hora(s) para sus horarios de asesoria.</h5>

    <table>

        <thead><tr>

                <th width="70" align='center' class="mensajeazul">HORA</th>
                <th width="70" align="center" class="mensajeazul">LUNES</th>
                <th width="70" align="center" class="mensajeazul">MARTES</th>
                <th width="70" align="center" class="mensajeazul">MIERCOLES</th>
                <th width="70" align="center" class="mensajeazul">JUEVES</th>
                <th width="70" align="center" class="mensajeazul">VIERNES</th>
                <th width="70" align="center" class="mensajeazul">SABADO</th>

            </tr></thead>
        <tbody>
            <%=filas%>

        </tbody>

    </table>
    <br>
    <input id="cierre" type="submit" value="Guardar Estado" onclick="guardarEstadoHorarios();" /><br><br>
    <br/>
    <a id="back" href="#" onclick="cargarInicioDocente();">VOLVER</a>

</div>