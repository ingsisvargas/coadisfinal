<%-- 
    Document   : Prueba_AJAX
    Created on : 15-may-2014, 14:56:50
    Author     : Rosemberg
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<jsp:useBean id="fachada" scope="page" class="Coadis.Facade.Facade"/>
<jsp:useBean id="doc" scope="page" class="Coadis.DTO.Docente"/>

<%
    doc.setCodigo(session.getAttribute("codigoD").toString());
    String materias = "<Select name='materia' id='materia' onchange='cargarGrupos();'> " + fachada.cargarMaterias(doc) + "</Select>";
    String tabla = fachada.cargarAsesoriasDia(doc);
%>


<div id="contenedorE" >
    <div>  <h3>Registro De Asesoria</h3> 

        <table>
            <tr><td>Materia: </td><td><%=materias%></td><td>Grupo: </td><td><select id="grupo" onchange="cargarTemas();"></select></td></tr>
            <tr><td>Tema: </td><td><select id="tema"></Select> </td><td>Estudiante:</td><td><input type="text" style="width:100px;" id="codEstudiante"/></td></tr>
            <tr><td>Descripcion:</td><td colspan="3"><textarea style="width:400px; height: 100px;" rezise="false" id="descripcion"></textarea></td></tr>
        </table>
        <br/>
        <br/>

        <input id="cierre" type="button" value="Registrar" onclick="registrarAsesoria();"/>

        <br/>  
        <br/>
        <h3>Asesorias Del Dia</h3>
        <br/>



        <div id="tablaAsesorias">
            <%=tabla%>
        </div>
        <br/>
        <br/>

    </div>
    <br/>
    <a id="back" href="#" onclick="cargarInicioDocente();">VOLVER</a>
    <br/>  <br/>
</div>
<br/>

<div class="dialogo" id="dialogo">

</div>


<br/>
