
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="fachada" scope="page" class="Coadis.Facade.Facade"/>
<jsp:useBean id="doc" scope="page" class="Coadis.DTO.Docente"/>

<%
    int val=Integer.parseInt(request.getParameter("val"));
    String docu="";
    int tipo=0;
    if(val==0){
    docu= "'" + session.getAttribute("codigoD").toString() + "'";
    }
    else {
    docu= "'" + session.getAttribute("codigoA").toString() + "'";    
    tipo=1;
    }

%>
<div id="contenedorD">
    <br/>
    <h3>Cambio de Contraseña</h3>
    <br/>
    <form id="formContrasena"  onsubmit="verificarContraseña(<%=docu+","+tipo%>);">
        <table>
            <tr><td><label>Contraseña anterior:</label></td><td> <input type="password" name="ante" id="ante"></td></tr>
            <tr><td><label>Contraseña nueva:</label></td><td> <input type="password" name="nueva" id="nueva"></td></tr>
            <tr><td><label>Confirmar contraseña:</label></td><td> <input type="password" name="repita" id="repita"></td></tr>
        </table>
        <br/><br/><input id="cierre" type="submit" value="Cambiar" />

    </form>
    <br/><br/>



    <a id="back" href="#" onclick="cargarInicioDocente();">Volver</a>
    <br/>
    <br/>

</div>
