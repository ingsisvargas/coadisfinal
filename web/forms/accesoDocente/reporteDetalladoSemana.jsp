<%-- 
    Document   : reporteDetalladoSemana
    Created on : 8/05/2014, 02:40:32 PM
    Author     : Luis Miguel Blanco
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="fachada" scope="page" class="Coadis.Facade.Facade"/>
<jsp:useBean id="doc" scope="page" class="Coadis.DTO.Docente"/>
<%
    doc.setCodigo(session.getAttribute("codigoD").toString());
    String materias = "<Select name='materia' id='materia' onchange='cargarGrupos()'>" + fachada.cargarMaterias(doc) + "</Select>";
   
    String mes=fachada.cargarMesesDocente(doc);
%>

<div id="contenedorD">
    <h3>REPORTE ASESORIA SEMANAL</h3>

    <br>
    
    <br/>
    <form target="_blank" action="../JSP/g_ReporteGrupoDocente.jsp" name="form1" onsubmit="return validaEnvia()">
        <table>
             <tr><td>Mes: <br><br></td><td><select name="mes"><%=mes%> </select><br><br></td></tr> 
        </table>

        <br>
        <input id="cierre" type="submit" value="Generar Reporte"/> <input id="cierre" type="reset" value="Limpiar"/>
    </form>
    <br>
    <a id="back" href="#" onclick="cargarInicioDocente();">VOLVER</a>
</div>

<script type="text/javascript">
    function validaEnvia() {

        if (document.form1.mes.selectedIndex == 0) {
            alert("Debe seleccionar un Mes.")
            document.form1.rango.focus()
            return false;
        }


        //el formulario se envia 

        return true;
    }

</script>
