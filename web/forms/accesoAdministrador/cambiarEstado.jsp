<%-- 
    Document   : ReporteGeneralPlanta
    Created on : 24-may-2014, 6:34:42
    Author     : Rosemberg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="fachada" scope="page" class="Coadis.Facade.Facade"/>
<div id="contenedorD">
    <h2>Cambio De Estado Docente</h2>

    <br/>


    <form>
        <table>
            <tr><td>Codigo Docente:</td><td><input type="text" id="codigo" /></td></tr>
            <tr><td>Estado:</td><td><select id="accion">
                        <option value="-1">--Seleccione Estado--</option>
                        <option value="0">Habilitar</option>
                        <option value="1">Deshabilitar</option>
                    </select></td></tr>
        </table>
        <br/><br/>
        <input id="cierre" type="button" value="Cambiar estado" onclick="cambiarEstado();"/>
    </form>
    <br>
    <br/>
    <a id="back" href="#" onclick="cargarInicioAdministrador();">VOLVER</a>
</div>
