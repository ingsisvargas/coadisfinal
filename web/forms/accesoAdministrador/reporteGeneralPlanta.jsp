<%-- 
    Document   : ReporteGeneralPlanta
    Created on : 24-may-2014, 6:34:42
    Author     : Rosemberg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="fachada" scope="page" class="Coadis.Facade.Facade"/>
<%

    String cadena = fachada.cargarMesesPlanta();

%>

<div id="contenedorD">
    <h3>REPORTE ASESORIA SEMANAL DOCENTES DE PLANTA</h3>

    <br/>
    <br/>
    <form target="_blank" action="../JSP/g_ReporteMensualPlanta.jsp" 
          name="form1" onsubmit="return validaEnvia()">

        <select name="mes">

            <%=cadena%>

        </select>
        <br/>
        <br/>
        <input id="cierre" type="submit" value="Generar Reporte"/>
    </form>
    <br/>
    <a id="back" href="#" onclick="cargarInicioAdministrador();">VOLVER</a>
</div>



<script type="text/javascript">
    function validaEnvia() {


        if (document.form1.mes.selectedIndex == 0) {
            alert("Debe seleccionar un MES.");
            document.form1.mes.focus();
            return false;
        }

        return true;
    }

</script>