/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.DTO;

/**
 *
 * @author Luis Miguel Blanco
 */
public class Vinculacion {
    
    private String codigo;
    private String vinculacion;

    public Vinculacion(String codigo, String vinculacion) {
        this.codigo = codigo;
        this.vinculacion = vinculacion;
    }

    public Vinculacion() {
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getVinculacion() {
        return vinculacion;
    }

    public void setVinculacion(String vinculacion) {
        this.vinculacion = vinculacion;
    }
    
    
    
}
