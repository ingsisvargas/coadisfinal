/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Coadis.DTO;

import java.util.Date;

/**
 *
 * @author SoftVargas
 */
public class Retardo {
    private int tiempoR;
    private Date fechaR;
    private Docente docenteR;

    public Retardo() {
    }

    public Retardo(int tiempoR, Date fechaR, Docente docenteR) {
        this.tiempoR = tiempoR;
        this.fechaR = fechaR;
        this.docenteR = docenteR;
    }

    public int getTiempoR() {
        return tiempoR;
    }

    public void setTiempoR(int tiempoR) {
        this.tiempoR = tiempoR;
    }

    public Date getFechaR() {
        return fechaR;
    }

    public void setFechaR(Date fechaR) {
        this.fechaR = fechaR;
    }

    public Docente getDocenteR() {
        return docenteR;
    }

    public void setDocenteR(Docente docenteR) {
        this.docenteR = docenteR;
    }
    
    
    
    
}
