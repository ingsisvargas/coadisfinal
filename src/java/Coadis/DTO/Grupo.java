/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Coadis.DTO;

import java.util.ArrayList;

/**
 *
 * @author SoftVargas
 */
public class Grupo {
    
    private int id;    
    private String nombre;
    private Docente docenteG;
    private Materia materia;

    public Grupo() {
    }

    public Grupo(String nombre, Docente docenteG,Materia materiaG) {
        this.nombre = nombre;
        this.docenteG = docenteG;
        this.materia=materiaG;
    
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Materia getMateria() {
        return materia;
    }

    public void setMateria(Materia materia) {
        this.materia = materia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Docente getDocenteG() {
        return docenteG;
    }

    public void setDocenteG(Docente docenteG) {
        this.docenteG = docenteG;
    }

  
    
    
    
    
}
