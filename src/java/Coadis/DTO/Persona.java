/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Coadis.DTO;

/**
 *
 * @author SoftVargas
 */
public class Persona {

    private String nombre;
    private String codigo;
    private String clave;
    
    
    public Persona() {
    }

    public Persona(String nombre,String codigo, String clave) {
        this.nombre = nombre;
    
       
        this.codigo=codigo;
        this.clave=clave;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

  
    
    
}
