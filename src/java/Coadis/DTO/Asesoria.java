/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Coadis.DTO;

import java.util.Date;

/**
 *
 * @author SoftVargas
 */
public class Asesoria {
    
    private Date fecha;
    private String estudianteA;
    private String descripcion;
    private Tema t;

    public Asesoria() {
    }

    public Asesoria(Date fecha,String estudiante, String descripcion) {
        this.fecha = fecha;
        this.estudianteA = estudiante;
        this.descripcion = descripcion;
     
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getEstudianteA() {
        return estudianteA;
    }

    public void setEstudianteA(String estudianteA) {
        this.estudianteA = estudianteA;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Tema getT() {
        return t;
    }

    public void setT(Tema t) {
        this.t = t;
    }
    

     
}
