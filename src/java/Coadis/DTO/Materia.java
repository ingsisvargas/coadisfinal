/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Coadis.DTO;

import java.util.ArrayList;

/**
 *
 * @author SoftVargas
 */
public class Materia {
    
    private String nombreM;
    private String codigoM;
    private ArrayList<Grupo> grupos;

    public Materia() {
    }

    public Materia(String nombreM, String codigoM) {
        this.nombreM = nombreM;
        this.codigoM = codigoM;
    }

    public String getNombreM() {
        return nombreM;
    }

    public void setNombreM(String nombreM) {
        this.nombreM = nombreM;
    }

    public String getCodigoM() {
        return codigoM;
    }

    public void setCodigoM(String codigoM) {
        this.codigoM = codigoM;
    }

    public ArrayList<Grupo> getGrupos() {
        return grupos;
    }

    public void setGrupos(ArrayList<Grupo> grupos) {
        this.grupos = grupos;
    }
    
   
    
    
}
