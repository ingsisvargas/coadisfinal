/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Coadis.DTO;
import Coadis.DTO.Docente;

import java.util.ArrayList;

/**
 *
 * @author SoftVargas
 */
public class Docente extends Persona{
    
    private boolean estado;
    private ArrayList<HorarioAsesoria> horarios;
    private ArrayList<Materia> materias;
    private Vinculacion vinculacion;
    private String email;

    public Docente() {
    }

    public Docente(String codigo, String clave, boolean estado, String nombre,Vinculacion vincu,String email) {
        super(nombre,codigo,clave);
        this.estado = estado;
        this.vinculacion=vincu;
        this.horarios= new ArrayList<>();
        this.email=email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<Materia> getMaterias() {
        return materias;
    }

    public void setMaterias(ArrayList<Materia> materias) {
        this.materias = materias;
    }

    public Vinculacion getVinculacion() {
        return vinculacion;
    }

    public void setVinculacion(Vinculacion vinculacion) {
        this.vinculacion = vinculacion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public ArrayList<HorarioAsesoria> getHorarios() {
        return horarios;
    }

    public void setHorarios(ArrayList<HorarioAsesoria> horarios) {
        this.horarios = horarios;
    }
    
    
}
