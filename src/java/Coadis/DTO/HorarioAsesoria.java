/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Coadis.DTO;

/**
 *
 * @author SoftVargas
 */
public class HorarioAsesoria {
    
    
    private int dia;
    private int hora;

    public HorarioAsesoria() {
    }

    public HorarioAsesoria(int dia, int hora) {
        this.dia = dia;
        this.hora = hora;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HorarioAsesoria other = (HorarioAsesoria) obj;
        if (this.dia != other.dia) {
            return false;
        }
        if (this.hora != other.hora) {
            return false;
        }
        return true;
    }
    
    
    
    
    
}
