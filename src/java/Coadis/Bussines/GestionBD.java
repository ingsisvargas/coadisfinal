/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.Bussines;

import Coadis.DAO.Docente_DAO;
import Coadis.DAO.Grupo_DAO;
import Coadis.DAO.Persona_DAO;
import Coadis.DAO.Semestre_DAO;
import Coadis.DAO.Vinculacion_DAO;
import Coadis.DTO.Docente;
import Coadis.DTO.Semestre;
import Coadis.Util.JCrypt;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luis Miguel Blanco
 */
public class GestionBD {

    public GestionBD() {
    }
    
   public String cargarBD(Semestre sem){
       
       try{
       new Semestre_DAO().registrarSemestre(sem);
       new Vinculacion_DAO().cargarVinculacionesXML(); 
       for(Docente d:new Docente_DAO().cargarDocentesXML()){

           
           
            d.setClave(JCrypt.crypt(JCrypt.salto(), "ufps"+d.getNombre().substring(0,2)));
            
            System.out.println(new Docente_DAO().registrarDocente(d)+"registro docente");
            System.out.println("Grupos de"+d.getCodigo());
            System.out.println(new Grupo_DAO().cargarGruposXML(d.getCodigo())+"grupos");
            
        }
        
       
 }catch(Exception e){e.printStackTrace();return "Carga Fallida";} 
 
    return "Carga Exitosa"; 
   } 

 
    
    

   
    
    
    
}
