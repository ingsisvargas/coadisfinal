/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.Bussines;

import Coadis.DAO.Tema_DAO;
import Coadis.DTO.Grupo;
import Coadis.DTO.Tema;
import java.util.ArrayList;

/**
 *
 * @author LUIS
 */
public class GestionTema {

    public GestionTema() {
    }


    public String registrarTema(Tema tema) {
        System.out.print(tema.getGrupo().getId());
        String msg="";
        if(new Tema_DAO().registrarTema(tema)){
            msg="Registro exitoso";
       
       }else msg="Falla en Registro";
        return msg;
    }

    public String cargarTemas(Grupo grupo) {
        ArrayList<Tema> temasCargados = new Tema_DAO().cargarTemasGrupo(grupo);
        String temas = "<option value='0'>--Seleccione Tema--</option>";

        for (Tema t : temasCargados) {
            temas += "<option value='" + t.getId() + "'>" + t.getDescripcion() + "</option>";
        }

        return temas;
    }
    
    
    public ArrayList<Tema> cargarTemasS(Grupo grupo){
        return  new Tema_DAO().cargarTemasGrupo(grupo);
    }

}
