/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.Bussines;

import Coadis.DAO.Asesoria_DAO;
import Coadis.DAO.Docente_DAO;
import Coadis.DAO.Grupo_DAO;
import Coadis.DAO.Materia_DAO;
import Coadis.DAO.Persona_DAO;
import Coadis.DAO.Retardo_DAO;
import Coadis.DAO.Tema_DAO;
import Coadis.DTO.Asesoria;
import Coadis.DTO.Docente;
import Coadis.DTO.Grupo;
import Coadis.DTO.Materia;
import Coadis.DTO.Retardo;
import Coadis.DTO.Tema;
import com.itextpdf.text.Document;
import com.itextpdf.text.FontFactory;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.Font;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luis Miguel Blanco
 */
public class GestionInformes {

    public GestionInformes() {
    }

    private String mes(int month) {

        switch (month) {

            case 1:
                return "ENERO";
            case 2:
                return "FEBRERO";
            case 3:
                return "MARZO";
            case 4:
                return "ABRIL";
            case 5:
                return "MAYO";
            case 6:
                return "JUNIO";
            case 7:
                return "JULIO";
            case 8:
                return "AGOSTO";
            case 9:
                return "SEPTIEMBRE";
            case 10:
                return "OCTUBRE";
            case 11:
                return "NOVIEMBRE";
            case 12:
                return "DICIEMBRE";
        }
        return "";
    }

    public String cargarMeses() {

        String meses = "<option value='0'>" + "Seleccione un Mes" + "</option>";
        Date hoy = new Date();
        hoy.setMonth(hoy.getMonth() + 1);
        int mesActual = hoy.getMonth();
        System.out.println(mesActual);
        for (int i = 1; i <= mesActual; i++) {
            switch (i) {
                case 1:
                    meses += "<option value='" + 1 + "'>Enero</option>";
                    break;
                case 2:
                    meses += "<option value='" + 2 + "'>Febrero</option>";
                    break;
                case 3:
                    meses += "<option value='" + 3 + "'>Marzo</option>";
                    break;
                case 4:
                    meses += "<option value='" + 4 + "'>Abril</option>";
                    break;
                case 5:
                    meses += "<option value='" + 5 + "'>Mayo</option>";
                    break;
                case 6:
                    meses += "<option value='" + 6 + "'>Junio</option>";
                    break;
                case 7:
                    meses += "<option value='" + 7 + "'>Julio</option>";
                    break;
                case 8:
                    meses += "<option value='" + 8 + "'>Agosto</option>";
                    break;
                case 9:
                    meses += "<option value='" + 9 + "'>Septiembre</option>";
                    break;
                case 10:
                    meses += "<option value='" + 10 + "'>Octubre</option>";
                    break;
                case 11:
                    meses += "<option value='" + 11 + "'>Noviembre</option>";
                    break;
                case 12:
                    meses += "<option value='" + 12 + "'>Diciembre</option>";
                    break;

            }
        }
        return meses;
    }

    public Document getInformeAsesoriaMensualCatedra(Document document, Image img, String mes) {

        Docente_DAO doc = new Docente_DAO();
        ArrayList<Docente> docCat = doc.getDocentesCatedra();

        document.open();
        document.addTitle("REPORTE MENSUAL DE ASESORIAS: DOCENTES DE CATEDRA");

        try {
            document.add(img);
            document.add(new Paragraph("\n"));

            Font f = new Font();
            f.setSize(14);
            f.setStyle(Font.BOLD);
            Paragraph parra = new Paragraph("REPORTE GENERAL DE ASESORIAS \nDOCENTES DE CATEDRA \nMES: " + mes((Integer.parseInt(mes))), f);
            parra.setAlignment(Element.ALIGN_CENTER);
            document.add(parra);
            document.add(new Paragraph("\n\n"));
            Paragraph parraDes = new Paragraph("DESCRIPCION: En el presente reporte se muestra un informe general sobre la actividad de cada uno de los docentes"
                    + " de catedra adscritos al Departamento de Sistemas e Informatica, indicando el numero de asesoramientos que realizo en el mes, por cada uno de los "
                    + "grupos de los que es tutor.", FontFactory.getFont("arial", 12));
            parraDes.setAlignment(Element.ALIGN_JUSTIFIED);

            document.add(parraDes);
            document.add(new Paragraph("\n\n"));

        } catch (DocumentException ex) {
            Logger.getLogger(GestionInformes.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Docente d : docCat) {

            ArrayList<Grupo> grupos = new Grupo_DAO().cargarGruposDocente(d);

            if (grupos.size() > 0) {
                PdfPTable miTabla = new PdfPTable(3);
                Paragraph parra = new Paragraph("NOMBRE: " + d.getNombre().toUpperCase() + "\n" + "CODIGO DOCENTE " + d.getCodigo(), FontFactory.getFont("arial", 16));
                parra.setAlignment(20);

                PdfPCell celda = new PdfPCell(parra);
                celda.setBorder(0);

                //unimos esta celda con otras 4
                celda.setColspan(3);
                //alineamos el contenido al centro
                celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                // añadimos un espaciado
                celda.setPadding(12.0f);
                //colocamos un color de fondo
                celda.setBackgroundColor(BaseColor.WHITE);
                miTabla.addCell(celda);

                celda = new PdfPCell(new Paragraph("MATERIA"));
                celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                celda.setBackgroundColor(BaseColor.GRAY);
                miTabla.addCell(celda);

                celda = new PdfPCell(new Paragraph("GRUPO"));
                celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                celda.setBackgroundColor(BaseColor.GRAY);
                miTabla.addCell(celda);

                celda = new PdfPCell(new Paragraph("CANTIDAD DE ASESORIAS"));
                celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                celda.setBackgroundColor(BaseColor.GRAY);
                miTabla.addCell(celda);

                int asesorias = 0;
                Date fecha = new Date();
                fecha.setYear(fecha.getYear() + 1900);
                fecha.setMonth(Integer.parseInt(mes));
                for (Grupo g : grupos) {
                    ArrayList<Tema> temas = new Tema_DAO().cargarTemasGrupo(g);
                    asesorias += new Asesoria_DAO().cantidadAsesoriasTemasGrupo(temas, fecha);

                    celda = new PdfPCell(new Paragraph(g.getMateria().getNombreM().toUpperCase()));
                    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                    miTabla.addCell(celda);

                    celda = new PdfPCell(new Paragraph(g.getNombre()));
                    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                    miTabla.addCell(celda);

                    celda = new PdfPCell(new Paragraph(asesorias + ""));
                    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                    miTabla.addCell(celda);

                }

                try {

                    document.add(miTabla);
                    document.add(new Paragraph("\n"));

                } catch (DocumentException ex) {

                    Logger.getLogger(GestionInformes.class.getName()).log(Level.SEVERE, null, ex);

                }

            }
        }
        document.close();
        return document;

    }

    public Document getInformeAsesoriaMensualPlanta(Document document, Image img, String mes) {

        Docente_DAO doc = new Docente_DAO();
        ArrayList<Docente> docCat = doc.getDocentesPlanta();

        document.open();
        document.addTitle("REPORTE MENSUAL DE ASESORIAS:  DOCENTES DE PLANTA");

        try {
            document.add(img);
            document.add(new Paragraph("\n"));

            Font f = new Font();
            f.setSize(14);
            f.setStyle(Font.BOLD);
            Paragraph parra = new Paragraph("REPORTE GENERAL DE ASESORIAS \nDOCENTES DE PLANTA \nMES: " + mes((Integer.parseInt(mes))), f);
            parra.setAlignment(Element.ALIGN_CENTER);
            document.add(parra);
            document.add(new Paragraph("\n\n"));
            Paragraph parraDes = new Paragraph("DESCRIPCION: En el presente reporte se muestra un informe general sobre la actividad de cada uno de los docentes"
                    + " de planta adscritos al Departamento de Sistemas e Informatica, indicando el numero de asesoramientos que realizo en el mes, por cada uno de los "
                    + "grupos de los que es tutor.", FontFactory.getFont("arial", 12));
            parraDes.setAlignment(Element.ALIGN_JUSTIFIED);

            document.add(parraDes);
            document.add(new Paragraph("\n\n"));

        } catch (DocumentException ex) {
            Logger.getLogger(GestionInformes.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Docente d : docCat) {

            ArrayList<Grupo> grupos = new Grupo_DAO().cargarGruposDocente(d);

            if (grupos.size() > 0) {
                PdfPTable miTabla = new PdfPTable(3);
                Paragraph parra = new Paragraph("NOMBRE: " + d.getNombre().toUpperCase() + "\n" + "CODIGO DOCENTE: " + d.getCodigo(), FontFactory.getFont("arial", 14));
                parra.setAlignment(20);

                PdfPCell celda = new PdfPCell(parra);
                celda.setBorder(0);
                //unimos esta celda con otras 4
                celda.setColspan(3);
                //alineamos el contenido al centro
                celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                // añadimos un espaciado
                celda.setPadding(12.0f);
                //colocamos un color de fondo
                celda.setBackgroundColor(BaseColor.WHITE);
                miTabla.addCell(celda);

                celda = new PdfPCell(new Paragraph("MATERIA"));
                celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                celda.setBackgroundColor(BaseColor.GRAY);
                miTabla.addCell(celda);

                celda = new PdfPCell(new Paragraph("GRUPO"));
                celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                celda.setBackgroundColor(BaseColor.GRAY);
                miTabla.addCell(celda);

                celda = new PdfPCell(new Paragraph("CANTIDAD DE ASESORIAS"));
                celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                celda.setBackgroundColor(BaseColor.GRAY);
                miTabla.addCell(celda);

                int asesorias = 0;
                Date fecha = new Date();
                fecha.setYear(fecha.getYear() + 1900);
                fecha.setMonth(Integer.parseInt(mes));
                for (Grupo g : grupos) {
                    ArrayList<Tema> temas = new Tema_DAO().cargarTemasGrupo(g);
                    asesorias += new Asesoria_DAO().cantidadAsesoriasTemasGrupo(temas, fecha);

                    celda = new PdfPCell(new Paragraph(g.getMateria().getNombreM().toUpperCase()));
                    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                    miTabla.addCell(celda);

                    celda = new PdfPCell(new Paragraph(g.getNombre()));
                    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                    miTabla.addCell(celda);

                    celda = new PdfPCell(new Paragraph(asesorias + ""));
                    celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                    miTabla.addCell(celda);

                }

                try {

                    document.add(miTabla);
                    document.add(new Paragraph("\n"));

                } catch (DocumentException ex) {

                    Logger.getLogger(GestionInformes.class.getName()).log(Level.SEVERE, null, ex);

                }

            }
        }
        document.close();
        return document;

    }

    public Document getInformeAsesoriaDetalladoDocente(Document document, Image img, Docente d, String mes) {

        Persona_DAO per = new Persona_DAO();

        d.setNombre(per.getAtributos(d.getCodigo())[0]);

        document.open();
        document.addTitle("REPORTE DE ASESORIAS DETALLADO");

        try {
            document.add(img);
            document.add(new Paragraph("\n"));
            Font f = new Font();
            f.setSize(14);
            f.setStyle(Font.BOLD);
            Paragraph parra = new Paragraph("REPORTE GENERAL DE ASESORIAS \nDOCENTES DE CATEDRA \nMES: " + mes((Integer.parseInt(mes))), f);
            parra.setAlignment(Element.ALIGN_CENTER);
            document.add(parra);
            document.add(new Paragraph("\n\n"));
            Paragraph parraDes = new Paragraph("DESCRIPCION: En el presente reporte se muestra un informe general sobre la actividad de cada uno de los docentes"
                    + " de catedra adscritos al Departamento de Sistemas e Informatica, indicando el numero de asesoramientos que realizo en el mes, por cada uno de los "
                    + "grupos de los que es tutor.", FontFactory.getFont("arial", 12));
            parraDes.setAlignment(Element.ALIGN_JUSTIFIED);

            document.add(parraDes);
            document.add(new Paragraph("\n\n"));

        } catch (DocumentException ex) {
            Logger.getLogger(GestionInformes.class.getName()).log(Level.SEVERE, null, ex);
        }

        PdfPTable miTabla = new PdfPTable(2);
        Paragraph parra = new Paragraph("Codigo del Docente: " + d.getCodigo() + "\n" + "Nombre: " + d.getNombre(), FontFactory.getFont("arial", 16));
        parra.setAlignment(20);

        PdfPCell celda = new PdfPCell(parra);
        //unimos esta celda con otras 4
        celda.setColspan(2);
        //alineamos el contenido al centro
        celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
        // añadimos un espaciado
        celda.setPadding(12.0f);
        //colocamos un color de fondo
        celda.setBackgroundColor(BaseColor.WHITE);
        miTabla.addCell(celda);

        celda = new PdfPCell(new Paragraph("Materia"));
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        miTabla.addCell(celda);

        celda = new PdfPCell(new Paragraph("Cantidad de asesorias"));
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        miTabla.addCell(celda);

        Materia_DAO mat = new Materia_DAO();

        ArrayList<Materia> materias = mat.materiasDocente(d);

        for (Materia mate : materias) {

            int asesorias = new Asesoria_DAO().buscarAsesoriasMateriasDocente(d, mate, mes);

            celda = new PdfPCell(new Paragraph(mate.getNombreM()));
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            miTabla.addCell(celda);

            celda = new PdfPCell(new Paragraph(asesorias + ""));
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            miTabla.addCell(celda);

        }

        try {

            document.add(miTabla);
            document.add(new Paragraph("\n\n"));

        } catch (DocumentException ex) {

            Logger.getLogger(GestionInformes.class.getName()).log(Level.SEVERE, null, ex);

        }

        document.close();
        return document;

    }

    public Document getInformeRetardo(Document document, Image img, Docente d, String mes) {
        Persona_DAO per = new Persona_DAO();

        d.setNombre(per.getAtributos(d.getCodigo())[0]);

        document.open();
        document.addTitle("REPORTE DE RETARDO");

        try {
            document.add(img);
            document.add(new Paragraph("\n"));

            Paragraph parra = new Paragraph("NOMBRE : " + d.getNombre(), FontFactory.getFont("arial", 14));

            parra.setAlignment(20);
            document.add(parra);

            Paragraph parra2 = new Paragraph("CODIGO : " + d.getCodigo(), FontFactory.getFont("arial", 14));

            parra.setAlignment(20);
            document.add(parra2);

            document.add(new Paragraph("\n"));

        } catch (DocumentException ex) {
            Logger.getLogger(GestionInformes.class.getName()).log(Level.SEVERE, null, ex);
        }

        PdfPTable miTabla = new PdfPTable(2);
        Paragraph parra = new Paragraph("Retardos", FontFactory.getFont("arial", 16));
        parra.setAlignment(20);

        PdfPCell celda = new PdfPCell(parra);
        //unimos esta celda con otras 4
        celda.setColspan(2);
        //alineamos el contenido al centro
        celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
        // añadimos un espaciado
        celda.setPadding(12.0f);
        //colocamos un color de fondo
        celda.setBackgroundColor(BaseColor.RED);
        miTabla.addCell(celda);

        celda = new PdfPCell(new Paragraph("Fecha del Retardo"));
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        miTabla.addCell(celda);

        celda = new PdfPCell(new Paragraph("Cantidad de tiempo Retardo"));
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        miTabla.addCell(celda);

        Retardo_DAO retardo = new Retardo_DAO();

        ArrayList<Retardo> lista = retardo.retardoDocente(d, mes);

        for (Retardo retardos : lista) {

            celda = new PdfPCell(new Paragraph(retardos.getFechaR().getYear() + "/" + retardos.getFechaR().getMonth() + "/" + retardos.getFechaR().getDate()));
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            miTabla.addCell(celda);

            celda = new PdfPCell(new Paragraph(retardos.getTiempoR() + ""));
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            miTabla.addCell(celda);

        }

        try {

            document.add(miTabla);
            document.add(new Paragraph("\n"));

        } catch (DocumentException ex) {

            Logger.getLogger(GestionInformes.class.getName()).log(Level.SEVERE, null, ex);

        }

        document.close();
        return document;
    }

    public Document getInformeAsesoriaGrupoDocente(Document document, Image img, Docente d, String mes) {

        Persona_DAO per = new Persona_DAO();
        Grupo_DAO grup = new Grupo_DAO();

        d.setNombre(per.getAtributos(d.getCodigo())[0]);

        ArrayList<Grupo> grupos = grup.cargarGruposDocente(d);

        document.open();
        document.addTitle("REPORTE DE ASESORIAS DEL DOCENTE");

        try {
            document.add(img);
            document.add(new Paragraph("\n"));

            Paragraph parra = new Paragraph("REPORTE INDICADORES DE USO\nMES: " + mes(Integer.parseInt(mes)), FontFactory.getFont("arial", 16, Font.BOLD));

            parra.setAlignment(Element.ALIGN_CENTER);
            document.add(parra);

            document.add(new Paragraph("\n\n"));
            Paragraph parraDes = new Paragraph("DESCRIPCION: En el presente reporte se muestra la informacion referente a los indicadores de uso por parte de los estudiantes respecto a las horas de asesoria"
                    + "en cada una de los grupos a cargo de un docente, teniendo una clasificacion por los temas y el detalle de las asesorias que el docente impartio en un"
                    + " mes.", FontFactory.getFont("arial", 12));
            parraDes.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(parraDes);
            document.add(new Paragraph("\n\n"));

            parra = new Paragraph("DOCENTE TUTOR: " + d.getNombre() + "\nCODIGO:" + d.getCodigo(), FontFactory.getFont("arial", 12, Font.BOLD, BaseColor.BLACK));
            document.add(parra);
            document.add(new Paragraph("\n\n"));

            parra = new Paragraph("DESCRIPCION DE INDICADORES", FontFactory.getFont("arial", 16, Font.BOLD));

            parra.setAlignment(Element.ALIGN_CENTER);
            document.add(parra);
            document.add(new Paragraph("\n\n"));

        } catch (DocumentException ex) {
            Logger.getLogger(GestionInformes.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Grupo g : grupos) {
            try {
                Paragraph parra = new Paragraph("MATERIA Y GRUPO: " + (g.getMateria().getNombreM() + "-" + g.getNombre()) + "", FontFactory.getFont("arial", 12, Font.BOLD, BaseColor.BLACK));
                document.add(parra);

                document.add(new Paragraph("\n\n\n"));

                parra = new Paragraph("TEMAS", FontFactory.getFont("arial", 12, Font.BOLD, BaseColor.BLACK));
                parra.setAlignment(Element.ALIGN_CENTER);
                document.add(parra);
                document.add(new Paragraph("\n\n"));

                ArrayList<Tema> temas = new Tema_DAO().cargarTemasGrupo(g);
                Date fecha = new Date();
                fecha.setYear(fecha.getYear() + 1900);
                fecha.setMonth(Integer.parseInt(mes));

                for (Tema t : temas) {
                    int canti = new Asesoria_DAO().buscarAsesoriasTema(t, fecha).size();

                    parra = new Paragraph("Tema: " + (t.getDescripcion()) + "\nIndicador de Uso: " + canti + " Asesorias", FontFactory.getFont("arial", 14, Font.NORMAL, BaseColor.BLACK));
                    document.add(parra);
                    ArrayList<Asesoria> asesorias = new Asesoria_DAO().buscarAsesoriasTema(t, fecha);
                    int c = 1;
                    if (asesorias.size() > 0) {
                        document.add(new Paragraph("\n"));

                        PdfPTable miTabla = new PdfPTable(4);

                        parra = new Paragraph("DETALLE\n\n", FontFactory.getFont("arial", 15, Font.BOLD, BaseColor.BLACK));
                        parra.setAlignment(Element.ALIGN_CENTER);
                        document.add(parra);

                        PdfPCell celda = new PdfPCell();
                        //unimos esta celda con otras 4

                        celda = new PdfPCell(new Paragraph("N°"));
                        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                        celda.setBackgroundColor(BaseColor.GRAY);
                        miTabla.addCell(celda);

                        celda = new PdfPCell(new Paragraph("Estudiante"));
                        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                        celda.setBackgroundColor(BaseColor.GRAY);
                        miTabla.addCell(celda);

                        celda = new PdfPCell(new Paragraph("Fecha"));
                        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                        celda.setBackgroundColor(BaseColor.GRAY);
                        miTabla.addCell(celda);

                        celda = new PdfPCell(new Paragraph("Descripcion"));
                        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                        celda.setBackgroundColor(BaseColor.GRAY);
                        miTabla.addCell(celda);

                        for (Asesoria a : asesorias) {

                            celda = new PdfPCell(new Paragraph(c + ""));
                            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                            miTabla.addCell(celda);

                            celda = new PdfPCell(new Paragraph(a.getEstudianteA()));
                            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                            miTabla.addCell(celda);

                            celda = new PdfPCell(new Paragraph(a.getFecha().toString()));
                            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                            miTabla.addCell(celda);

                            celda = new PdfPCell(new Paragraph(a.getDescripcion()));
                            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
                            miTabla.addCell(celda);
                            c++;
                        }

                        document.add(miTabla);
                        document.add(new Paragraph("\n"));

                    }
                }

            } catch (Exception e) {

            }

        }

        document.close();
        return document;

    }

}
