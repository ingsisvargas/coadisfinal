/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.Bussines;

import Coadis.DAO.Materia_DAO;
import Coadis.DTO.Docente;
import Coadis.DTO.Materia;
import java.util.ArrayList;

/**
 *
 * @author Estudiante
 */
public class GestionMateria {

    public GestionMateria() {
    
    }
    
    public String cargarMaterias(Docente doc) {
        Materia_DAO materia = new Materia_DAO();
        ArrayList<Materia> materias = materia.materiasDocente(doc);
        String res = "<option value=0>-Seleccione Materia</option>";
        if(materias.isEmpty())
            return "<option value=0>No existen grupos</option>";
        for (Materia m : materias) {
            res += "<option value='" + m.getCodigoM() + "'>"    + m.getNombreM() + "</option>";
        }


        return res;
    }

    
    
}
