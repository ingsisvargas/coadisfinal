/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.Bussines;

import Coadis.DAO.Docente_DAO;
import Coadis.DAO.HorarioAsesoria_DAO;
import Coadis.DAO.Persona_DAO;
import Coadis.DTO.Docente;
import Coadis.DTO.HorarioAsesoria;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Estudiante
 */
public class GestionHorario {

    public GestionHorario() {
    }

    public int getCantidadHorasAsignadas(Docente doc) {

        Docente_DAO docDao = new Docente_DAO();

        ArrayList<HorarioAsesoria> array = docDao.getHorariosAsesoria(doc);

        return array.size();

    }

    public boolean registrarHorarioAsesoria(String[] horas, Docente obj) {

        Docente_DAO doc = new Docente_DAO();
        doc.eliminarHorariosSemestre(obj);

        for (int i = 0; i < horas.length; i++) {
            String[] val = horas[i].split("-");
            int hora = Integer.parseInt(val[0]);
            int dia = Integer.parseInt(val[1]);
            boolean c = doc.registrarHorarioAsesoria(new HorarioAsesoria(dia, hora), obj);
            System.out.println(c);
            if (!c) {
                return false;
            }
        }

        return true;
    }

    public boolean horarioPermitido(Docente doc) {
        Docente_DAO docenteDao = new Docente_DAO();
        ArrayList<HorarioAsesoria> horarios = docenteDao.getHorariosAsesoria(doc);
        Date fechaAct = new Date();

        for (HorarioAsesoria hor : horarios) {
            if (hor.getDia() == fechaAct.getDay() && hor.getHora() == fechaAct.getHours()) {
                return true;
            }

        }
        return false;

    }

    public String getHorarioAsesoria(Docente doc) {

        Docente_DAO dc = new Docente_DAO();
        ArrayList<HorarioAsesoria> horarios = dc.getHorariosAsesoria(doc);

        String filas = "";

        for (int i = 6; i <= 21; i++) {
            String fila = "<tr><th colspan'2' align='center'>" + i + ":00</t>";
            for (int j = 1; j <= 6; j++) {
                fila += "<td colspan'2' align='center'><input  id='switch" + i + "" + j + "' type='checkbox' name='hora' class='input-switch' value='" + i + "-" + j + "' onClick='maximoChecked(" + "\"switch" + i + "" + j + "\");' /><label for='switch" + i + "" + j + "'> <span class='grip'>  </span></label></td>\n";
                if (horarios.indexOf(new HorarioAsesoria(j, i)) >= 0) {
                    fila = fila.replaceAll("<td colspan'2' align='center'><input class='input-switch' id='switch" + i + "" + j + "", "<td colspan'2' align='center'><input class='input-switch' id='switch" + i + "" + j + "");
                    fila = fila.replaceAll("value='" + i + "-" + j + "' onClick", "checked=true  value='" + i + "-" + j + "' onClick");
                }
            }
            fila += "</tr>";
            filas += fila;
        }
        return filas;
    }

    public String consultaHorarioDocente(Docente doc) {
        Docente_DAO docente = new Docente_DAO();
        if (docente.validarIngreso(doc)) {
            String[] d = new Persona_DAO().getAtributos(doc.getCodigo());
            doc.setNombre(d[0]);
            ArrayList<HorarioAsesoria> horarios = docente.getHorariosAsesoria(doc);
            String res = "<table><tr><th>Nombre: </th><td>" + doc.getNombre() + "</td> <th>Codigo: </th><td>" + doc.getCodigo() + "</td><th>TipoDocente</th><td>TC</td></tr>";
            res += "<tr><th colspan=3>DIA</th><th colspan=3>HORA</th></tr>";

            for (HorarioAsesoria hor : horarios) {
                res += "<tr><td id='cent' colspan=3>" + this.nombreDia(hor.getDia()) + "</td><td id='cent' colspan=3>" + hor.getHora() + "</td></tr>";

            }
            return res + "</table>";

        }

        return "El docente no existe";

    }

    private String nombreDia(int dia) {
        switch (dia) {
            case 1:
                return "Lunes";
            case 2:
                return "Martes";
            case 3:
                return "Miercoles";
            case 4:
                return "Jueves";
            case 5:
                return "Viernes";
            case 6:
                return "Sabado";
        }

        return "";
    }

}
