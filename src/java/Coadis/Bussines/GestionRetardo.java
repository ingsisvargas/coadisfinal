/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.Bussines;

import Coadis.DAO.Docente_DAO;
import Coadis.DAO.Retardo_DAO;
import Coadis.DTO.Docente;
import Coadis.DTO.HorarioAsesoria;
import Coadis.DTO.Persona;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Estudiante
 */
public class GestionRetardo {

    public GestionRetardo() {
    }
    
    
      public boolean validarRetraso(Persona D) {

        Docente_DAO doc = new Docente_DAO();
        Docente d = new Docente();
        d.setCodigo(D.getCodigo());
        ArrayList<HorarioAsesoria> horarios = doc.getHorariosAsesoria(d);
        Date fecha = new Date();

        

        for (HorarioAsesoria h : horarios) {
            if (h.getDia() == fecha.getDay()) {
                if (fecha.getMinutes() > 15) {
                    Retardo_DAO ret = new Retardo_DAO();
                    return ret.registrarRetardo(d, fecha);
                }

            }
        }
        return false;
    }
    
}
