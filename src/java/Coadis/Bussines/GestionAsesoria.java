/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.Bussines;

import Coadis.DAO.Asesoria_DAO;
import Coadis.DTO.Asesoria;
import Coadis.DTO.Docente;
import Coadis.DTO.Tema;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Estudiante
 */
public class GestionAsesoria {

    public GestionAsesoria() {
    }
    
    
    public int registrarAsesoria(String est, Tema tema, String descripcion) {
     //  Estudiante_DAO estudiante= new Estudiante_DAO();
       Date fechaAct=new Date();
       fechaAct.setMonth(fechaAct.getMonth()+1);
       fechaAct.setYear(fechaAct.getYear()+1900);
   //    if(estudiante.validarEstudiante(est)){
           Asesoria_DAO asesoria=new Asesoria_DAO();
           Asesoria nueva= new Asesoria();
           nueva.setT(tema);
           nueva.setDescripcion(descripcion);
           nueva.setEstudianteA(est);
           nueva.setFecha(fechaAct);
           return asesoria.registrarAsesoria(nueva);  
   //    }
     
    }

    public String cargarAsesoriasDia(Docente doc) {
        ArrayList<Asesoria> asesorias=this.cargarInfoAsesoriasDia(doc);
        int i=1;
        String msj="<table border='1' id='asesoriasDia'>"
                + "<tr><th>Orden</th><th>Materia</th><th>Grupo</th><th>Estudiante</th><th>Tema Aclarado</th><th>Descripcion</th></tr>";
        
        for(Asesoria as:asesorias){
            msj+="<tr>\n"
                    + "<td>"+i+"</td>\n"
                    + "<td>"+as.getT().getGrupo().getMateria().getNombreM()+"</td>\n"
                    + "<td>"+as.getT().getGrupo().getNombre()+"</td>\n"
                    + "<td>"+as.getEstudianteA()+"</td>\n"
                    + "<td>"+as.getT().getDescripcion()+"</td>\n"
                    + "<td><center><input type='button' value='ver' onclick='visualizarDescripcionAsesoria(\""+as.getDescripcion()+"\");'/></center></td>\n"
                    + "</tr>";   
            i++;
          
        }
        
        msj+="</table>";
        
        return msj;
    }

    /**
     * Para la gestion del webService
     * @param doc
     * @return 
     */
    
    public ArrayList<Asesoria> cargarInfoAsesoriasDia(Docente doc) {
       ArrayList<Asesoria> asesorias= new Asesoria_DAO().cargarAsesoriasDia(doc);
       return asesorias;
    }
    
    
}
