/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.Bussines;

import Coadis.DAO.Grupo_DAO;
import Coadis.DTO.Docente;
import Coadis.DTO.Grupo;
import Coadis.DTO.Materia;
import java.util.ArrayList;

/**
 *
 * @author Estudiante
 */
public class GestionGrupo {

    public GestionGrupo() {
    }
    
    
    public String cargarGrupos(Materia m) {
        Grupo_DAO grupo = new Grupo_DAO();
        ArrayList<Grupo> grupos = grupo.cargarGrupos(m);
        
        if(grupos.isEmpty())
            return "<option value=0>No existen grupos</option>";
        
        String res = "<option value=0>-seleccione grupo-</option>";
        for (Grupo g : grupos) {
            res += "<option value='" + g.getId() + "'>" + g.getNombre() + "</option>";
        }

        return res;
    }

    public ArrayList<Grupo> cargarGruposDocente(Docente d){
        return new Grupo_DAO().cargarGruposDocente(d);
    }
    
    
}
