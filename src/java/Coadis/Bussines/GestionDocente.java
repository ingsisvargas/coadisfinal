/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Coadis.Bussines;

import Coadis.DAO.Docente_DAO;
import Coadis.DAO.Persona_DAO;
import Coadis.DTO.Administrador;
import Coadis.DTO.Docente;
import Coadis.Util.JCrypt;

/**
 *
 * @author Rosemberg
 */
public class GestionDocente {
    
    public GestionDocente(){
        
    }
    
    public boolean cambiarEstado(Docente e, String tipo){
        
        Docente_DAO d=new Docente_DAO();
        if(d.validarIngreso(e)){
       return d.cambiarEstado(e, tipo);
        }
        return false;
    }

    public String cambiarContrasena(Docente doc, String pass) {
  doc.setClave(JCrypt.crypt(JCrypt.salto(),pass));
    boolean b=new Persona_DAO().cambiarContrasena(doc,pass);
    if(b)
        return "Cambio realizado";
    else
        return "Error en la conexion";
    }

public String cambiarContrasena(Administrador doc, String pass) {
  doc.setClave(JCrypt.crypt(JCrypt.salto(), pass));
    boolean b=new Persona_DAO().cambiarContrasena(doc,pass);
    if(b)
        return "Cambio realizado";
    else
        return "Error en la conexion";
    }
    
}
