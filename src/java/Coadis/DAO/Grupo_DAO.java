/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.DAO;

import Coadis.DTO.Docente;
import Coadis.DTO.Grupo;
import Coadis.DTO.Materia;
import Coadis.DTO.Semestre;
import Coadis.Util.BaseDeDatos;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.jdom2.Element;
import org.jdom2.input.DOMBuilder;

/**
 *
 * @author SoftVargas
 */
public class Grupo_DAO {

    public Grupo_DAO() {
    }

    public boolean cargarGruposXML(String codigo) throws Exception {

        String url = "http://dptosist.ufps.edu.co/piagev1/servlet/piagev.servlets.HojaDeVidaProfesor?codigo=" + codigo;

        Materia_DAO materia_DAO = new Materia_DAO();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dombuilder = factory.newDocumentBuilder();
        //Carga el documento desde la URL :

        org.w3c.dom.Document w3cDocument = dombuilder.parse(url);

        //Construye el objeto DOM
        DOMBuilder jdomBuilder = new DOMBuilder();
        org.jdom2.Document jdomDocument = jdomBuilder.build(w3cDocument);

        org.jdom2.Element nodoRaiz;
        nodoRaiz = jdomDocument.getRootElement();
        org.jdom2.Element x = nodoRaiz.getChild("catedra");
        List<Element> grupos = x.getChildren("grupo");

        for (org.jdom2.Element elemento : grupos) {
            System.out.println(grupos.size());
            org.jdom2.Element materia = elemento.getChild("materia");

            String nombreMateria = materia.getChild("nombre").getValue();
            String codigoMteria = materia.getChild("codigo").getValue();

            String codigoG = elemento.getChild("codigo").getValue();

            org.jdom2.Element planEstu = materia.getChild("plandeestudios");
            String codigoPlanEstu = planEstu.getChild("codigo").getValue();

            Docente doc = new Docente();
            doc.setCodigo(codigo);

            Materia mat = new Materia();
            mat.setCodigoM(codigoPlanEstu + codigoMteria);
            mat.setNombreM(nombreMateria);

            if (materia_DAO.noExiste(mat)) {
                materia_DAO.registrarMateria(mat);
            }
            registrarGrupo(new Grupo(codigoG, doc, mat));

        }
        return true;
    }

    public boolean registrarGrupo(Grupo g) {
        Semestre sem = new Semestre_DAO().cargarSemestreActual();
        String sql = "INSERT INTO grupo(nombre, materia, docente,semestre)  VALUES ('" + g.getNombre() + "', '" + g.getMateria().getCodigoM() + "','" + g.getDocenteG().getCodigo() + "', " + sem.getId() + ")";

        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }
        System.out.println(sql);

        return BaseDeDatos.ejecutarActualizacionSQL(sql);

    }

    public ArrayList<Grupo> cargarGrupos(Materia m) {
        Semestre sem = new Semestre_DAO().cargarSemestreActual();
        String sql = "Select nombre, id from grupo where materia='" + m.getCodigoM() + "' AND semestre=" + sem.getId();
        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        ArrayList<Grupo> grupos = new ArrayList<Grupo>();
        ResultSet rs = BaseDeDatos.ejecutarSQL(sql);
        try {
            while (rs.next()) {
                Grupo g = new Grupo();
                g.setNombre(rs.getString(1));
                g.setId(rs.getInt(2));
                grupos.add(g);

            }
        } catch (SQLException ex) {
            Logger.getLogger(Administrador_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return grupos;
    }

    public Grupo cargarGrupo(int id) {
        String sql = "Select * from grupo where id=" + id;
        Grupo g = new Grupo();
        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        ResultSet rs = BaseDeDatos.ejecutarSQL(sql);
        try {
            while (rs.next()) {
              g.setId(rs.getInt("id"));
              g.setNombre(rs.getString("nombre"));
              g.setMateria(new  Materia_DAO().cargarMateria(rs.getString("materia")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Grupo_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        BaseDeDatos.desconectar();
        return g;

    }

    public ArrayList<Grupo> cargarGruposDocente(Docente d) {
      Semestre sem= new Semestre_DAO().cargarSemestreActual();
      ArrayList<Grupo> grupos= new ArrayList<>();
      String sql="Select id, nombre, materia from grupo where docente='"+d.getCodigo()+"' AND semestre="+sem.getId();
      if(!BaseDeDatos.hayConexion()){
          BaseDeDatos.conectar();
      }
      
      ResultSet rs=BaseDeDatos.ejecutarSQL(sql);
        try {
            while(rs.next()){
                Grupo g= new Grupo();
                g.setId(rs.getInt("id"));
                g.setNombre(rs.getString("nombre"));
                g.setMateria(new Materia_DAO().cargarMateria(rs.getString("materia")));
                grupos.add(g);
            } } catch (SQLException ex) {
            Logger.getLogger(Grupo_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
      
      
      BaseDeDatos.desconectar();
    return grupos;
    }
}
