/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.DAO;

import Coadis.DTO.Docente;
import Coadis.DTO.Retardo;
import Coadis.Util.BaseDeDatos;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SoftVargas
 */
public class Retardo_DAO {

    public Retardo_DAO() {
    }

    public boolean registrarRetardo(Docente D, Date fecha) {
        int tiempo = fecha.getMinutes() - 15;
        fecha.setYear(fecha.getYear()+1900);
        fecha.setMonth(fecha.getMonth()+1);
        String Sql = "Insert into retraso(id,fecha,tiempo_retraso,docente) values (3,'"
                + fecha.getYear() + "/" + fecha.getMonth() + "/" + fecha.getDate() + "',"
                + tiempo + ",'" + D.getCodigo() + "')";
        
        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }
        
        return BaseDeDatos.ejecutarActualizacionSQL(Sql);
    }
    
    public ArrayList<Integer> getFechasRetardos(Docente doc) {
        
        String sentencia="Select fecha from retraso A where"
                + " A.docente = '"+doc.getCodigo()+"'";
        
          if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }
          ArrayList<Integer> fechas=new ArrayList<Integer>();
          
          ResultSet rs=BaseDeDatos.ejecutarSQL(sentencia);

          if(rs==null){
              return null;
          }
          
        try {
            while(rs.next()){
                
                Date d=rs.getDate(1);
                if(!fechas.contains(d.getMonth()+1)){                    
                fechas.add(d.getMonth()+1);
                }            
            }
            if(fechas.isEmpty()){
                return null;
            }
            return fechas;
        } catch (SQLException ex) {
                System.out.println("entra a acá");
            Logger.getLogger(Asesoria_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
        
    }

    public ArrayList<Retardo> retardoDocente(Docente d, String mes) {
    
        String sql="Select * from retraso R where R.docente='"+d.getCodigo()+"'"
                +" AND R.fecha BETWEEN '2014/"+(mes)+"/01' AND '2014/"+(mes)+"/31'"; 

       
        ArrayList<Retardo> fechas=new ArrayList<Retardo>();
          
          ResultSet rs=BaseDeDatos.ejecutarSQL(sql);

          if(rs==null){
              return null;
          }
          
        try {
            while(rs.next()){
                Docente e=new Docente();
                e.setCodigo(rs.getString(2));
                Date fe= rs.getDate(4);
                fe.setYear(fe.getYear()+1900);
                fe.setMonth(fe.getMonth()+1);
                System.out.println(rs.getInt(3));
                fechas.add(new Retardo(rs.getInt(3), fe, e));
                
    }
            return fechas;
        }catch(SQLException ex) {
            Logger.getLogger(Asesoria_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;

    }

}
