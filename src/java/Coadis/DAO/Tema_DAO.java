/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.DAO;

import Coadis.DTO.Docente;

import Coadis.DTO.Grupo;
import Coadis.DTO.Materia;
import Coadis.DTO.Tema;
import Coadis.Util.BaseDeDatos;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SoftVargas
 */
public class Tema_DAO {

    public Tema_DAO() {
    }
    
    public boolean registrarTema(Tema  tema){
        System.out.print(tema.getGrupo().getId());
        String sql="insert into tema (descripcion, grupo) values ('"+tema.getDescripcion()+"','"+tema.getGrupo().getId()+"')";
        System.out.println(sql);
        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }
        try {
            if (BaseDeDatos.ejecutarActualizacionSQL(sql)) {
                return true;
            }

            BaseDeDatos.desconectar();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Tema cargarTema(int idTema) throws SQLException {
        String sql = "Select * from tema where id=" + idTema;
        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        ResultSet rs = BaseDeDatos.ejecutarSQL(sql);
        Tema t = new Tema();
        while (rs.next()) {
            t.setDescripcion(rs.getString("descripcion"));
            t.setGrupo(new Grupo_DAO().cargarGrupo(rs.getInt("grupo")));
            t.setId(rs.getInt("id"));
        }

        BaseDeDatos.desconectar();

        return t;
    }

    public ArrayList<Tema> cargarTemasGrupo(Grupo grupo) {
        String sql = "select * from tema where grupo=" + grupo.getId();
        ArrayList<Tema> temas = new ArrayList<>();

        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        ResultSet rs = BaseDeDatos.ejecutarSQL(sql);
        try {
            while (rs.next()) {
                Tema t = new Tema();
                t.setId(rs.getInt("id"));
                t.setDescripcion(rs.getString("descripcion"));
                temas.add(t);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Tema_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        BaseDeDatos.desconectar();

        return temas;
    }

}
