/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.DAO;

import Coadis.Util.BaseDeDatos;
import Coadis.DTO.Docente;
import Coadis.DTO.HorarioAsesoria;
import Coadis.DTO.Persona;
import Coadis.DTO.Vinculacion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.jdom2.Element;
import org.jdom2.input.DOMBuilder;

/**
 *
 * @author SoftVargas
 */
public class Docente_DAO {

    public Docente_DAO() {
    }

    public ArrayList<Docente> cargarDocentesXML() throws Exception {
        ArrayList<Docente> lista = new ArrayList<Docente>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dombuilder = factory.newDocumentBuilder();
        //Carga el documento desde la URL :

        org.w3c.dom.Document w3cDocument = dombuilder.parse("http://dptosist.ufps.edu.co/piagev1/servlet/piagev.servlets.Profesor?dpto=52");

        //Construye el objeto DOM
        DOMBuilder jdomBuilder = new DOMBuilder();
        org.jdom2.Document jdomDocument = jdomBuilder.build(w3cDocument);

        org.jdom2.Element nodoRaiz;
        nodoRaiz = jdomDocument.getRootElement();
        org.jdom2.Element x = nodoRaiz.getChild("profesores");
        List<Element> l = x.getChildren("profesor");
        for (org.jdom2.Element elemento : l) {
            String nombre = elemento.getChild("nombre").getValue();
            String codigo = elemento.getChild("codigo").getValue();
            String email = elemento.getChild("correoelectronico").getValue();
            String vincul = elemento.getChild("vinculacion").getValue();
            Vinculacion vin = new Vinculacion();
            vin.setCodigo(vincul);

            Docente nueva = new Docente(codigo, "cl", true, nombre, vin, email);

            lista.add(nueva);
        }

        return (lista);
    }

    public boolean registrarDocente(Docente doc) {

        String sql = "INSERT INTO docente( codigo, tipo, estado_e, email, vinculacion) VALUES ('" + doc.getCodigo() + "', " + 1 + "," + doc.isEstado() + " , '" + doc.getEmail() + "', '" + doc.getVinculacion().getCodigo() + "')";
        if (!BaseDeDatos.hayConexion()) {

            BaseDeDatos.conectar();
            System.out.println("hubo en docente");
        }
        boolean per = new Persona_DAO().registrarPersona(doc);
        System.out.println(per + "1");

        return (BaseDeDatos.ejecutarActualizacionSQL(sql));

    }

    public boolean validarIngreso(Persona obj) {
        String sql = "Select * from  docente D  where  D.codigo='" + obj.getCodigo() + "'";

        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        ResultSet rs = BaseDeDatos.ejecutarSQL(sql);
        try {
            if (rs.next()) {
                BaseDeDatos.desconectar();
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Administrador_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        BaseDeDatos.desconectar();
        return false;
    }

    public ArrayList<HorarioAsesoria> getHorariosAsesoria(Docente obj) {
        return new HorarioAsesoria_DAO().getHorariosByDocente(obj);
    }

    public boolean registrarHorarioAsesoria(HorarioAsesoria horarioA, Docente obj) {
        HorarioAsesoria_DAO horario = new HorarioAsesoria_DAO();
        return horario.registrarHorarioAsesoria(horarioA, obj);
    }

    public ArrayList<Docente> getDocentesCatedra() {

        String cad = "select P.nombre, P.codigo from docente D, persona P where P.codigo=D.codigo AND vinculacion='CT'";

        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        ResultSet rs = BaseDeDatos.ejecutarSQL(cad);

        ArrayList<Docente> listado = new ArrayList<Docente>();

        try {

            while (rs.next()) {

                String nombre = rs.getString(1);
                String codigo = rs.getString(2);

                Docente d = new Docente();
                d.setCodigo(codigo);
                d.setNombre(nombre);

                listado.add(d);

            }

            return listado;
        } catch (SQLException ex) {
            Logger.getLogger(Administrador_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public ArrayList<Docente> getDocentesPlanta() {

        String cad = "select P.nombre, P.codigo from docente D, persona P where P.codigo=D.codigo AND vinculacion='TC'";

        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        ResultSet rs = BaseDeDatos.ejecutarSQL(cad);

        ArrayList<Docente> listado = new ArrayList<Docente>();

        try {

            while (rs.next()) {

                String nombre = rs.getString(1);
                String codigo = rs.getString(2);

                Docente d = new Docente();
                d.setCodigo(codigo);
                d.setNombre(nombre);

                listado.add(d);

            }

            return listado;
        } catch (SQLException ex) {
            Logger.getLogger(Administrador_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public boolean cambiarEstado(Docente e, String tipo) {

        boolean valor = false;

        if (tipo.equals("0")) {
            valor = true;
        } else if (tipo.equals("0")) {
            valor = false;
        }

        String sql = "Update docente set estado_e=" + valor + " where codigo='" + e.getCodigo() + "'";

        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        return BaseDeDatos.ejecutarActualizacionSQL(sql);

    }

    public boolean isPermitido(Docente e) {

        String sql = "Select estado_e from docente D where D.codigo='" + e.getCodigo() + "'";

        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        ResultSet rs = BaseDeDatos.ejecutarSQL(sql);
        try {
            if (rs.next()) {

                return rs.getBoolean(1);

            }
        } catch (SQLException ex) {
            Logger.getLogger(Docente_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;

    }


    public void eliminarHorariosSemestre(Docente doc) {
    new HorarioAsesoria_DAO().eliminarHorarios(doc);
    }


}
