    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.DAO;

import Coadis.DTO.Administrador;
import Coadis.DTO.Persona;
import Coadis.Util.BaseDeDatos;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SoftVargas
 */
public class Administrador_DAO {

    public Administrador_DAO() {
    }

    public boolean validarIngreso(Persona obj) {

        String sql = "Select * FROM administrador where codigo='" + obj.getCodigo() + "'";

        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        ResultSet rs = BaseDeDatos.ejecutarSQL(sql);
        try {
            if (rs.next()) {
                BaseDeDatos.desconectar();
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Administrador_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }


        BaseDeDatos.desconectar();
        return false;
        
    }
}
