/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.DAO;

import Coadis.DTO.Docente;
import Coadis.DTO.HorarioAsesoria;
import Coadis.DTO.Materia;
import Coadis.DTO.Semestre;
import Coadis.Util.BaseDeDatos;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SoftVargas
 */
public class Materia_DAO {

    public Materia_DAO() {
    }

    public boolean registrarMateria(Materia mat) {
        String sql = "insert into materia (codigo, nombre) values ('" + mat.getCodigoM() + "','" + mat.getNombreM() + "')";
        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        return (BaseDeDatos.ejecutarActualizacionSQL(sql));

    }

    public ArrayList<Materia> materiasDocente(Docente doc) {
        Semestre sem = new Semestre_DAO().cargarSemestreActual();
        String sql = "select  nombre , codigo from materia where codigo IN(select  materia from grupo where docente='" + doc.getCodigo() + "' AND semestre=" + sem.getId() + " ) ORDER BY nombre";
        System.out.println(sql);
        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        ResultSet rs = BaseDeDatos.ejecutarSQL(sql);
        ArrayList<Materia> materias;
        materias = new ArrayList<>();
        try {
            while (rs.next()) {
                Materia mat = new Materia(rs.getString(1), rs.getString(2));
                materias.add(mat);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Materia_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return materias;

    }

    public boolean noExiste(Materia mat) {
        String sql = "select count(*) as mat from materia where  codigo='" + mat.getCodigoM()+"'";
        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }
        int val =0;
        ResultSet rs = BaseDeDatos.ejecutarSQL(sql);
        try {
            while(rs.next()){
            val = rs.getInt("mat");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Materia_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return (val==0);
    }

    public Materia cargarMateria(String materia) {
      String sql = "Select * from materia where codigo='" + materia+"'";
        Materia  m = new Materia();
        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        ResultSet rs = BaseDeDatos.ejecutarSQL(sql);
        try {
            while (rs.next()) {
              m.setNombreM(rs.getString("nombre"));
              m.setCodigoM(rs.getString("codigo"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Grupo_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        BaseDeDatos.desconectar();
        return m;
    
    
    }
}
