/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.DAO;

import Coadis.DTO.Docente;
import Coadis.DTO.HorarioAsesoria;
import Coadis.DTO.Persona;
import Coadis.DTO.Semestre;
import Coadis.Util.BaseDeDatos;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rosemberg
 */
public class HorarioAsesoria_DAO {

    public HorarioAsesoria_DAO() {
    }

    public ArrayList<HorarioAsesoria> getHorariosByDocente(Docente obj) {

        ArrayList<HorarioAsesoria> horarios = new ArrayList<HorarioAsesoria>();
        Semestre sem = new Semestre_DAO().cargarSemestreActual();
        String sql = "Select hora,dia from horario_atencion h  where h.docente='" + obj.getCodigo() + "' AND semestre=" + sem.getId() + " ORDER BY dia ASC";

        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        ResultSet rs = BaseDeDatos.ejecutarSQL(sql);
        try {
            if (rs != null) {
                while (rs.next()) {
                    HorarioAsesoria nuevo = new HorarioAsesoria();
                    nuevo.setHora(rs.getInt(1));
                    nuevo.setDia(rs.getInt(2));
                    horarios.add(nuevo);

                }
            }
            return horarios;
        } catch (SQLException ex) {
            Logger.getLogger(HorarioAsesoria_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public boolean registrarHorarioAsesoria(HorarioAsesoria horario, Docente obj) {
        Semestre sem = new Semestre_DAO().cargarSemestreActual();
        String sql = "insert into horario_atencion (dia, hora, docente,semestre) values("
                + horario.getDia() + "," + horario.getHora() + ",'" + obj.getCodigo() + "'," + sem.getId() + ")";
        
        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }
        return BaseDeDatos.ejecutarActualizacionSQL(sql);
    }

    public ArrayList<HorarioAsesoria> consultaHorarioDocente(Docente doc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void eliminarHorarios(Docente doc) {
        Semestre sem = new Semestre_DAO().cargarSemestreActual();
        String sql = "DELETE from horario_atencion where semestre=" + sem.getId() + " AND docente='" + doc.getCodigo() + "'";

        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }
        BaseDeDatos.ejecutarActualizacionSQL(sql);
        BaseDeDatos.desconectar();

    }
}
