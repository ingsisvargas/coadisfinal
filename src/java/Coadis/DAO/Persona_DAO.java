/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Coadis.DAO;

import Coadis.DTO.Administrador;
import Coadis.DTO.Docente;
import Coadis.DTO.Persona;
import Coadis.Util.BaseDeDatos;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SoftVargas
 */
public class Persona_DAO {

    public Persona_DAO() {
    }

     public boolean registrarPersona(Persona user){
        
     String sql="INSERT INTO persona( nombre, codigo, clave) VALUES ('"+user.getNombre()+"', '"+user.getCodigo()+"', '"+user.getClave()+"')";
        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        if(BaseDeDatos.ejecutarActualizacionSQL(sql)){
            System.out.println(" persona registro");
              return true;}
        
        return false;
    
    }
    
    public String[] getAtributos(String codigo) {

        String sentencia="select * from persona P where P.codigo='"+codigo+"'";
        System.out.println(sentencia);
        ResultSet rs=BaseDeDatos.ejecutarSQL(sentencia);

            try{
                
            if(rs.next()){
                String atributos[]= new String[3];
            
                atributos[0]=rs.getString(1);
                atributos[1]=rs.getString(2);
                atributos[2]=rs.getString(3);
                
                return atributos;
                }
            }
            catch(Exception e){
        
                    System.out.println(codigo);
                
            }
        
        return null;
        
    }
    
public boolean validarIngreso(Persona user,int tipo) {
        String sql="select * from persona where codigo='"+user.getCodigo()+"'";
        if(!BaseDeDatos.hayConexion())
            BaseDeDatos.conectar();
        
       ResultSet rs=BaseDeDatos.ejecutarSQL(sql);
        try {
            if(rs.next()){
                if(tipo==1){
                    Administrador_DAO adm= new Administrador_DAO();
                    user.setNombre(rs.getString("nombre"));
                    return (adm.validarIngreso(user));         
               }
                if(tipo==2){
                  Docente_DAO docente= new Docente_DAO();
                  user.setNombre(rs.getString("nombre"));
                  return docente.validarIngreso(user);
                 
                }
                    
            }
        } catch (SQLException ex) {
            Logger.getLogger(Administrador_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    
    }

        public Persona getPersona(Persona user) {
          Persona p=new Persona();
        try {
            String sql="select * from persona where codigo='"+user.getCodigo()+"'";
            System.out.println(sql);
                   if(!BaseDeDatos.hayConexion())
                       BaseDeDatos.conectar();
                   
                  ResultSet rs=BaseDeDatos.ejecutarSQL(sql);
                  
                  if(rs.next()){
              
                  p.setClave(rs.getString("clave"));
                  p.setCodigo(rs.getString("codigo"));
                  p.setNombre(rs.getString("nombre"));
                  }
                  else
                      return null;
        } catch (SQLException ex) {
            Logger.getLogger(Persona_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    return p;
    }

    
    public boolean cambiarContrasena(Docente doc, String pass) {

        String sql = "Update persona set clave=" + pass + " where codigo='" + doc.getCodigo() + "'";

        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        return BaseDeDatos.ejecutarActualizacionSQL(sql);
    
    }
    
    public boolean cambiarContrasena(Administrador doc, String pass) {

        String sql = "Update persona set clave=" + pass + " where codigo='" + doc.getCodigo() + "'";

        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        return BaseDeDatos.ejecutarActualizacionSQL(sql);
    
    }
    
}
