/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.DAO;

import Coadis.DTO.Docente;
import Coadis.DTO.Vinculacion;
import Coadis.Util.BaseDeDatos;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.jdom2.Element;
import org.jdom2.input.DOMBuilder;

/**
 *
 * @author Luis Miguel Blanco
 */
public class Vinculacion_DAO {

    public Vinculacion_DAO() {
    }
    
    
    public  boolean cargarVinculacionesXML() throws Exception
    {
     
       DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
       DocumentBuilder dombuilder = factory.newDocumentBuilder();
        //Carga el documento desde la URL :
       
        org.w3c.dom.Document w3cDocument = dombuilder.parse("http://dptosist.ufps.edu.co/piagev1/servlet/piagev.servlets.Profesor?dpto=52");

       //Construye el objeto DOM
        DOMBuilder jdomBuilder = new DOMBuilder();
        org.jdom2.Document jdomDocument = jdomBuilder.build(w3cDocument);
        
        org.jdom2.Element nodoRaiz;
        nodoRaiz = jdomDocument.getRootElement();
        org.jdom2.Element x=nodoRaiz.getChild("vinculaciones");
        List<Element> l=x.getChildren("vinculacion");
        for(org.jdom2.Element elemento:l)
        {
            String nombre=elemento.getChild("nombre").getValue();
            String codigo=elemento.getChild("codigo").getValue();
            
          this.registrarVinculacion(new Vinculacion(codigo,nombre));
         
        }
return true;
    }

    
    
    private boolean registrarVinculacion(Vinculacion vinc) {
     String sql="insert into vinculacion (tipo,descripcion) values ('"+vinc.getCodigo()+"','"+vinc.getVinculacion()+"')";
        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }    
    return BaseDeDatos.ejecutarActualizacionSQL(sql);
    }

    
}
