/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Coadis.DAO;

import Coadis.DTO.Asesoria;
import Coadis.DTO.Docente;
import Coadis.DTO.Grupo;
import Coadis.DTO.Materia;
import Coadis.DTO.Tema;
import Coadis.Util.BaseDeDatos;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SoftVargas
 */
public class Asesoria_DAO {

    public Asesoria_DAO() {
    }
    

    public int registrarAsesoria(Asesoria a) {
        
        String sql="insert into asesoria (estudiante, descripcion,tema,fecha) values ('"+a.getEstudianteA()+"','"+a.getDescripcion()+"','"+a.getT().getId()+"','"+a.getFecha().getYear()+"/"+a.getFecha().getMonth()+"/"+a.getFecha().getDate()+"')";
        
        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        if(BaseDeDatos.ejecutarActualizacionSQL(sql))
            
        return 1;
        
        return 0;

    }

    
     public  ArrayList<Asesoria> buscarAsesoriasPorRango(Docente doc, Materia mat, Date d, int dia1, int dia2) {
        
        d.setYear(d.getYear()+1900);
        d.setMonth(d.getMonth()+1);
      
        String sql="Select * from asesoria A  where A.materia='"+mat.getCodigoM()+"'AND A.fecha BETWEEN DATE '"+d.getYear()+"-0"+d.getMonth()+"-0"+dia1+"'AND DATE '"+d.getYear()+"-0"+d.getMonth()+"-0"+dia2+"'";
         System.out.println(sql);
        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }

        ResultSet rs = BaseDeDatos.ejecutarSQL(sql);
        
        ArrayList<Asesoria>asesorias=new ArrayList<Asesoria>();
        try {
            while(rs.next()){
                String e=rs.getString("estudiante");
                Date fe= rs.getDate("fecha");
                fe.setYear(fe.getYear()+1900);
                fe.setMonth(fe.getMonth()+1);
                asesorias.add(new Asesoria(fe, e, rs.getString("descripcion")));       
            }
            System.out.println(asesorias.size());
            return asesorias;
        } catch (SQLException ex) {
            Logger.getLogger(Asesoria_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }        
       
        return null;    
        
    }

    public ArrayList<Integer> getFechasCatedra() {
        
        String sentencia="Select fecha from asesoria A, docente D where"
                + " A.docente = D.codigo AND D.vinculacion <> 'TC'";;
        
          if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }
          ArrayList<Integer> fechas=new ArrayList<Integer>();
          
          ResultSet rs=BaseDeDatos.ejecutarSQL(sentencia);
         
          if(rs==null)
              return null;
         
        try {
            while(rs.next()){
                
                Date d=rs.getDate(1);
                
                if(!fechas.contains(d.getMonth()+1)){                    
                fechas.add(d.getMonth()+1);
                }            
            }
             if(fechas.size() ==0){
                return null;
            }
            return fechas;
        } catch (SQLException ex) {
                
            Logger.getLogger(Asesoria_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
        
    }
    
        public ArrayList<Integer> getFechasPlanta() {
        
        String sentencia="Select fecha from asesoria A, docente D where"
                + " A.docente = D.codigo AND D.vinculacion = 'TC'";
        
          if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }
          ArrayList<Integer> fechas=new ArrayList<Integer>();
          
          ResultSet rs=BaseDeDatos.ejecutarSQL(sentencia);

          if(rs==null){
               System.out.println("NULL");
              return null;
          }
          
        try {
            while(rs.next()){
                System.out.println("uff");
                Date d=rs.getDate(1);
                if(!fechas.contains(d.getMonth()+1)){                    
                fechas.add(d.getMonth()+1);
                }            
            }
            
            if(fechas.size() ==0){
                               System.out.println("NULL");

                return null;
            }
            return fechas;
        } catch (SQLException ex) {
                System.out.println("entra a acá");
            Logger.getLogger(Asesoria_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
        
    }

    public int buscarAsesoriasPorRango(Docente doc, String mes, int ini, int fin) {
        
        if(mes.length()<1){
            
        }
        String sentencia="Select id from asesoria A where A.docente='"+doc.getCodigo()+"' "
                + "AND A.fecha BETWEEN '2014/"+(mes)+"/"+(ini)+"' AND '2014/"+(mes)+"/"+(fin)+"'";
        
        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }
        ResultSet rs = BaseDeDatos.ejecutarSQL(sentencia);
        
        if(sentencia==null){
            return 0;
        }
        
        int cont=0;
        try {
            while(rs.next())
                cont++;
            
            return cont;
            
        } catch (SQLException ex) {
                
            Logger.getLogger(Asesoria_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return 0;
    }

    public int buscarAsesoriasMateriasDocente(Docente d, Materia mate, String mes) {
        Date fecha=new Date();
        fecha.setYear(fecha.getYear()+1900);
        int año=fecha.getYear();
        int fin=this.diasMes(Integer.parseInt(mes),año);
        
        
        String cad="Select id from asesoria A where A.docente='"+d.getCodigo()+"' "
                + "AND A.materia='"+mate.getCodigoM()+"' AND A.fecha BETWEEN '"+año+"/"+(mes)+"/01' AND '"+año+"/"+(mes)+"/"+fin+"'";
        
        
        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }
        ResultSet rs = BaseDeDatos.ejecutarSQL(cad);
        
        if(rs==null){
            return 0;
        }
        
        int cont=0;
        
        try {
            while(rs.next())
                cont++;
            return cont;
            
        } catch (SQLException ex) {
                
            Logger.getLogger(Asesoria_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return 0;
        
    }

    public ArrayList<Integer> getFechasDocente(Docente doc) {
    
          String sentencia="Select fecha from asesoria A where"
                + " A.docente ='"+doc.getCodigo()+"'";
        
          if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }
          ArrayList<Integer> fechas=new ArrayList<Integer>();
          
          ResultSet rs=BaseDeDatos.ejecutarSQL(sentencia);

          if(rs==null){
              return null;
          }
          
        try {
            while(rs.next()){
                
                Date d=rs.getDate(1);
                if(!fechas.contains(d.getMonth()+1)){                    
                fechas.add(d.getMonth()+1);
                }            
            }
            if(fechas.size() ==0){
                return null;
            }
            return fechas;
        } catch (SQLException ex) {
                System.out.println("entra a acá");
            Logger.getLogger(Asesoria_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
        
        
    }

     private int diasMes(int mes,int año) {
         int dias=0;
         switch (mes) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                dias = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                dias = 30;
                break;
            case 2:
                if ( ((año % 4 == 0) && !(año % 100 == 0))
                     || (año % 400 == 0) )
                 dias    = 29;
                else
                    dias = 28;
                break;
             
        }
     return dias;
    }
     
     

    public ArrayList<Asesoria> cargarAsesoriasDia(Docente doc) {
        
        Date d= new Date();
        d.setYear(d.getYear()+1900);
        d.setMonth(d.getMonth()+1);
        String sql="Select * from asesoria where fecha='"+d.getYear()+"-"+d.getMonth()+"-"+d.getDate()+"'"
                + "AND tema IN(Select id from tema where grupo IN(Select id from grupo where docente='"+doc.getCodigo()+"'))";
        System.out.println(sql);
        ArrayList<Asesoria> asesorias=new ArrayList<>();
        
        if(!BaseDeDatos.hayConexion()){
            BaseDeDatos.conectar();
        }
        
        ResultSet rs=BaseDeDatos.ejecutarSQL(sql);
        try {
            while(rs.next()){
            Asesoria as= new Asesoria();
            as.setDescripcion(rs.getString("descripcion"));
            as.setFecha(rs.getDate("fecha"));
            as.setEstudianteA(rs.getString("estudiante"));
            Tema t=new Tema_DAO().cargarTema(rs.getInt("tema"));
            as.setT(t);
            asesorias.add(as);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Asesoria_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        BaseDeDatos.desconectar();
        
        
    return asesorias;
    }

    
    public int cantidadAsesoriasTemasGrupo(ArrayList<Tema> temas,Date fecha){
     
        if(!BaseDeDatos.hayConexion()){
            BaseDeDatos.conectar();
        }
        
        int asesorias=0;
        
        for(Tema t:temas){
        String sql="Select count(*) from asesoria A where A.tema='"+t.getId()+"' AND A.fecha BETWEEN '"+fecha.getYear()+"-"+fecha.getMonth()+"-01' AND '"+fecha.getYear()+"-"+fecha.getMonth()+"-"+this.diasMes(fecha.getMonth(),fecha.getYear())+"'";
            ResultSet rs=BaseDeDatos.ejecutarSQL(sql);
            try {
                while(rs.next()){
                    asesorias+=rs.getInt("count");
                }
            } catch (SQLException ex) {
                Logger.getLogger(Asesoria_DAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
        BaseDeDatos.desconectar();
        return asesorias;
    }


    public ArrayList<Asesoria> buscarAsesoriasTema(Tema t,Date fecha) {

        if(!BaseDeDatos.hayConexion()){
            BaseDeDatos.conectar();
        }
          ArrayList<Asesoria> asesorias=new ArrayList<Asesoria>();
          
            String sql="Select * from asesoria A where A.tema='"+t.getId()+"' AND A.fecha BETWEEN '"+fecha.getYear()+"-"+fecha.getMonth()+"-01' AND '"+fecha.getYear()+"-"+fecha.getMonth()+"-"+this.diasMes(fecha.getMonth(),fecha.getYear())+"'";
   ResultSet rs=BaseDeDatos.ejecutarSQL(sql);
            try {
                while(rs.next()){
                    asesorias.add(new Asesoria(rs.getDate(2), rs.getString(1), rs.getString(3)));
                    System.out.println("Agrego a lla lista");
                }
            } catch (SQLException ex) {
                Logger.getLogger(Asesoria_DAO.class.getName()).log(Level.SEVERE, null, ex);
            }
    return asesorias;
    }
    
        

    
}
