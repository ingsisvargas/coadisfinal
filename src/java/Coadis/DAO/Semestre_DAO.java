/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Coadis.DAO;

import Coadis.DTO.Materia;
import Coadis.DTO.Semestre;
import Coadis.Util.BaseDeDatos;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 *
 * @author SoftVargas
 */
public class Semestre_DAO {

    public Semestre_DAO() {
    }
    
    
    public Semestre cargarSemestreActual(){
         String sql = "select id from semestre ORDER BY id desc limit 1"; 

        if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }
        
        Semestre sem= new Semestre();

        ResultSet rs = BaseDeDatos.ejecutarSQL(sql);
      
        try {
            while (rs.next()) {
               sem.setId(rs.getInt("id"));
            }
        } catch (SQLException ex) {
          
        }
        BaseDeDatos.desconectar();

        return sem;
        
    }
    
    public void registrarSemestre(Semestre sem){
        String sql="insert into semestre (año,periodo) values("+sem.getAño()+",'"+sem.getPeriodo()+"')";
         if (!BaseDeDatos.hayConexion()) {
            BaseDeDatos.conectar();
        }
         
         BaseDeDatos.ejecutarActualizacionSQL(sql);
         BaseDeDatos.desconectar();
    }
    
}
