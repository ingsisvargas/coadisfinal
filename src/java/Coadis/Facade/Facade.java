/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Coadis.Facade;

import Coadis.Bussines.*;
import Coadis.DTO.*;
import com.itextpdf.text.Document;

/**
 *
 * @author SoftVargas
 */
public class Facade {

    public Facade() {
    }
public String cambiarContrasena(Docente doc,String pass){
    return new GestionDocente().cambiarContrasena(doc,pass);
}

    
    public String cargarTemas(Grupo grupo){
        return new GestionTema().cargarTemas(grupo);
    }


public String cambiarContrasena(Administrador doc,String pass){
    return new GestionDocente().cambiarContrasena(doc,pass);
}



    public String verificarContraseña(Docente doc,String pass){
        return new GestionAcceso().verificarContrasena(doc, pass);

    }
    
    public String cargarAsesoriasDia(Docente doc){
        return new GestionAsesoria().cargarAsesoriasDia(doc);
    }

    public String registrarTema(Tema tema) {
        String msg = "";
        System.out.print(tema.getGrupo().getId());
        msg = new GestionTema().registrarTema(tema);

        return msg;
    }

    public String cargarBD(Semestre sem) {
        return new GestionBD().cargarBD(sem);
    }

    public String validarIngreso(Persona user, int tipo) {
        return new GestionAcceso().validarIngresoEncriptado(user, tipo);
    }

    public int getCantidadHorasAsignadas(Docente doc) {
        return new GestionHorario().getCantidadHorasAsignadas(doc);
    }

    public boolean registrarHorarioAsesoria(String[] horas, Docente obj) {
        return new GestionHorario().registrarHorarioAsesoria(horas, obj);
    }

    public boolean horarioPermitido(Docente doc) {
        return new GestionHorario().horarioPermitido(doc);
    }

    public String getHorarioAsesoria(Docente doc) {
        return new GestionHorario().getHorarioAsesoria(doc);
    }

    public String cargarMaterias(Docente doc) {
        return new GestionMateria().cargarMaterias(doc);
    }

    public String cargarGrupos(Materia m) {

        return new GestionGrupo().cargarGrupos(m);
    }

    public int registrarAsesoria(String est, Tema tema, String descripcion) {

        return new GestionAsesoria().registrarAsesoria(est,tema, descripcion);

    }

    public String[] cargarSemanas() {
        return null;
    }

    public boolean validarRetraso(Persona D) {
        return new GestionRetardo().validarRetraso(D);
    }

    public Document generarPDFGrupoDocente(Docente doc, Document docum, String mes, com.itextpdf.text.Image img) {
        return new GestionInformes().getInformeAsesoriaGrupoDocente(docum, img, doc, mes);

    }

    public Document generarPDFMesualCatedra(Document docum, String mes, com.itextpdf.text.Image img) {

        return new GestionInformes().getInformeAsesoriaMensualCatedra(docum, img, mes);

    }

    public Document generarPDFMesualPlanta(Document docum, String mes, com.itextpdf.text.Image img) {

        return new GestionInformes().getInformeAsesoriaMensualPlanta(docum, img, mes);

    }

    public Document generarPDFDetalladoDocente(Document docum, Docente d, com.itextpdf.text.Image img, String mes) {

        return new GestionInformes().getInformeAsesoriaDetalladoDocente(docum, img, d, mes);

    }

    public Document generarPDFRetardo(Document docum, Docente d, com.itextpdf.text.Image img, String mes) {

        return new GestionInformes().getInformeRetardo(docum, img, d, mes);

    }

    public String[] getDatos(Persona doc) {
        return new GestionAcceso().getDatos(doc);
    }

    public String consultaHorarioDocente(String codigo) {
        Docente doc = new Docente();
        doc.setCodigo(codigo);
        return new GestionHorario().consultaHorarioDocente(doc);
    }

    public String cargarMesesCatedra() {

        return new GestionInformes().cargarMeses();

    }

    public String cargarMesesPlanta() {

        return new GestionInformes().cargarMeses();

    }

    public String cargarMesesDocente(Docente d) {

        return new GestionInformes().cargarMeses();
    }

    public String cargarMesesRetardos(Docente d) {

        return new GestionInformes().cargarMeses();

    }

    public boolean activarDoc(Docente d, String tipo) {

        return new GestionDocente().cambiarEstado(d, tipo);

    }

    public String isValido(Docente d) {

        return new GestionAcceso().isPermitido(d);

    }
}
