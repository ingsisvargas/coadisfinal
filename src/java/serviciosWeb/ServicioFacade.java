/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviciosWeb;

import Coadis.Bussines.GestionAcceso;
import Coadis.Bussines.GestionAsesoria;
import Coadis.Bussines.GestionGrupo;
import Coadis.Bussines.GestionTema;
import Coadis.DTO.Docente;
import Coadis.DTO.Grupo;
import Coadis.DTO.Persona;
import Coadis.DTO.Tema;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.json.*;

/**
 *
 * @author SoftVargas
 */
@WebService(serviceName = "ServicioFacade")
public class ServicioFacade {

    /**
     * Permite Cargar los grupos donde un docente es tutor
     */
    @WebMethod(operationName = "cargarGrupos")
    public String cargarGrupos(@WebParam(name = "codigoDocente") String codigoDocente) {
        Docente d = new Docente();
        d.setCodigo(codigoDocente);
        ArrayList<Grupo> grupos = new GestionGrupo().cargarGruposDocente(d);
        JSONObject todos=new JSONObject();
        JSONArray list = new JSONArray();
        int i=0;
        for (Grupo g : grupos) {
            
            JSONObject json = new JSONObject();
            try {
                json.append("id", g.getId());
                json.append("materia", g.getMateria().getNombreM());
                json.append("nombreGrupo",g.getNombre());
                list.put(i, json);
                i++;
            } catch (JSONException ex) {
                Logger.getLogger(ServicioFacade.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            todos.append("grupos", list);
        } catch (JSONException ex) {
            Logger.getLogger(ServicioFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return todos.toString();
    }

    /**
     * Permite Cargar los temas registrados para un grupo
     */
    @WebMethod(operationName = "cargarTemas")
    public String cargarTemas(@WebParam(name = "idGrupo") String idGrupo) {
    Grupo g= new Grupo();
    g.setId(Integer.parseInt(idGrupo));
    ArrayList<Tema> temas=new GestionTema().cargarTemasS(g);
    JSONObject todos=new JSONObject();
    JSONArray list= new  JSONArray();
    int i=0;
    for(Tema t:temas){
        try {
            JSONObject json=new JSONObject();
            json.append("id",t.getId());
            json.append("descripcion",t.getDescripcion());
            list.put(i,json);
            i++;
        } catch (JSONException ex) {
            Logger.getLogger(ServicioFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        try {
            todos.append("temas", list);
        } catch (JSONException ex) {
            Logger.getLogger(ServicioFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return todos.toString();
    }

    /**
     * Permite efectuar el registro de una Asesoria
     */
    @WebMethod(operationName = "registrarAsesoria")
    public String registrarAsesoria(@WebParam(name = "idTema") String idTema, @WebParam(name = "estudiante") String estudiante, @WebParam(name = "descripcion") String descripcion) {
       Tema t=new Tema();
       t.setId(Integer.parseInt(idTema));
       int r=new GestionAsesoria().registrarAsesoria(estudiante, t, descripcion);
       JSONObject json=new JSONObject();
        try {
            json.append("resultado",r);
        } catch (JSONException ex) {
            Logger.getLogger(ServicioFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return json.toString();
    }

    /**
     * Permite efectuar el registro de un Tema
     */
    @WebMethod(operationName = "registrarTema")
    public String registrarTema(@WebParam(name = "idGrupo") String idGrupo, @WebParam(name = "descripcion") String descripcion) {
          Grupo g=new Grupo();
          g.setId(Integer.parseInt(idGrupo));
          Tema t= new Tema();
          t.setGrupo(g);
          t.setDescripcion(descripcion);
          String msj=new GestionTema().registrarTema(t);
          JSONObject json=new JSONObject();
        try {
            json.append("resultado", msj);
        } catch (JSONException ex) {
            Logger.getLogger(ServicioFacade.class.getName()).log(Level.SEVERE, null, ex);
        }

        return json.toString();
    }

    /**
     * Permite gestionar el Inicio de Sesion en el sistema
     */
    @WebMethod(operationName = "inicioSesion")
    public String inicioSesion(@WebParam(name = "codigo") String codigo, @WebParam(name = "pass") String pass) {
       Persona p=new Persona();
       p.setCodigo(codigo);
       p.setClave(pass);
       String msj=new GestionAcceso().validarIngresoEncriptado(p,2);
       JSONObject json=new JSONObject();
        try {
            json.append("resultado", msj);
        } catch (JSONException ex) {
            Logger.getLogger(ServicioFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return json.toString();
    }
}
